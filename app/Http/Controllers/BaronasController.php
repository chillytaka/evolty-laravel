<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\baronas;
use App\User;
use Laracsv\Export;
use Illuminate\Support\Facades\Hash;
use DateTime;
use DateTimeZone;

class BaronasController extends Controller
{
    //
    public function edit()
    {
        $user = Auth::user();

        $baronas = baronas::where('users_id', $user->id)->get();

        if ($baronas->first()) {
            return view('baronas_edit', ['baronas' => $baronas[0], 'user' => $user->name]);
        } else {
            return view('baronas_tambah', ['user' => $user->name]);
        }
    }
    

    public function store(Request $request)
    {

        $this->validate($request, [
            'namatim' => 'required',
            'kategorilomba' => 'required',
            'asalinstansi' => 'required',
            'alamatinstansi' => 'required',
            'nama_anggota1' => 'required',
            'phonenumber' => 'required|digits_between:10,13',
            'filektmanggota1' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota2' => 'required_with:nama_anggota2|file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota3' => 'required_with:nama_anggota3|file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota4' => 'required_with:nama_anggota4|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota1' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota2' => 'required_with:filektmanggota2|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota3' => 'required_with:filektmanggota3|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota4' => 'required_with:filektmanggota4|file|image|mimes:jpeg,png,jpg|max:2048',
            'filebuktipembayaran' => 'required|file|image|mimes:jpeg,png,jpg|max:2048'
        ], [], [
            'namatim' => 'Nama Tim',
            'kategorilomba' => "Kategori Lomba",
            'asalinstansi' => 'Asal Instansi',
            'alamatinstansi' => "Alamat Instansi",
            'nama_anggota1' => "Nama Anggota 1",
            'nama_anggota2' => "Nama Anggota 2",
            'nama_anggota3' => "Nama Anggota 3",
            'nama_anggota4' => "Nama Anggota 4",
            'phonenumber' => 'Phone Number',
            'filektmanggota1' => "File Kartu Pelajar / KTP Anggota 1",
            'filektmanggota2' => "File Kartu Pelajar / KTP Anggota 2",
            'filektmanggota3' => "File Kartu Pelajar / KTP Anggota 3",
            'filektmanggota4' => "File Kartu Pelajar / KTP Anggota 4",
            'filefotoanggota1' => "File Foto Anggota 1",
            'filefotoanggota2' => "File Foto Anggota 2",
            'filefotoanggota3' => "File Foto Anggota 3",
            'filefotoanggota4' => "File Foto Anggota 4",
            'filebuktipembayaran' => "File Bukti Pembayaran",
        ]);


        $namafoto2 = '';
        $namafoto3 = '';
        $namafoto4 = '';
        $namaktm1 = '';
        $namaktm2 = '';
        $namaktm3 = '';
        $namaktm4 = '';

        if ($request->file('filebuktipembayaran') != "") {
            $buktiBayar = $request->file('filebuktipembayaran');
            $namafilebayar = $request->namatim . '-BAYAR-' . time() . '.' . $buktiBayar->getClientOriginalExtension();
            $buktiBayar->move(public_path() . '/baronas_file/buktibayar/', $namafilebayar);
        }

        if ($request->file('filektmanggota1') != "") {
            $ktm1 = $request->file('filektmanggota1');
            $namaktm1 = $request->namatim . '-KTM-' . time() . '.' . $ktm1->getClientOriginalExtension();
            $ktm1->move(public_path() . '/baronas_file/ktm_anggota1/', $namaktm1);

            $foto1 = $request->file('filefotoanggota1');
            $namafoto1 = $request->namatim . '-FOTO-' . time() . '.' . $foto1->getClientOriginalExtension();
            $foto1->move(public_path() . '/baronas_file/foto_anggota1/', $namafoto1);
        }

        if ($request->file('filektmanggota2') != "") {
            $ktm2 = $request->file('filektmanggota2');
            $namaktm2 = $request->namatim . '-KTM-' . time() . '.' . $ktm2->getClientOriginalExtension();
            $ktm2->move(public_path() . '/baronas_file/ktm_anggota2/', $namaktm2);

            $foto2 = $request->file('filefotoanggota2');
            $namafoto2 = $request->namatim . '-FOTO-' . time() . '.' . $foto2->getClientOriginalExtension();
            $foto2->move(public_path() . '/baronas_file/foto_anggota2/', $namafoto2);
        }

        if ($request->file('filektmanggota3') != "") {
            $ktm3 = $request->file('filektmanggota3');
            $namaktm3 = $request->namatim . '-KTM-' . time() . '.' . $ktm3->getClientOriginalExtension();
            $ktm3->move(public_path() . '/baronas_file/ktm_anggota3/', $namaktm3);

            $foto3 = $request->file('filefotoanggota3');
            $namafoto3 = $request->namatim . '-FOTO-' . time() . '.' . $foto3->getClientOriginalExtension();
            $foto3->move(public_path() . '/baronas_file/foto_anggota3/', $namafoto3);
        }

        if ($request->file('filektmanggota4') != "") {
            $ktm4 = $request->file('filektmanggota4');
            $namaktm4 = $request->namatim . '-KTM-' . time() . '.' . $ktm4->getClientOriginalExtension();
            $ktm4->move(public_path() . '/baronas_file/ktm_anggota4/', $namaktm4);

            $foto4 = $request->file('filefotoanggota4');
            $namafoto4 = $request->namatim . '-FOTO-' . time() . '.' . $foto4->getClientOriginalExtension();
            $foto4->move(public_path() . '/baronas_file/foto_anggota4/', $namafoto4);
        }

        baronas::create([
            'users_id' => $userId = Auth::user()->id,
            'namatim' => $request->namatim,
            'kategori' => $request->kategorilomba,
            'asalinstansi' => $request->asalinstansi,
            'alamatinstansi' => $request->alamatinstansi,
            'namapembimbing' => $request->nama_pembimbing,
            'namaanggota1' => $request->nama_anggota1,
            'namaanggota2' => $request->nama_anggota2,
            'namaanggota3' => $request->nama_anggota3,
            'namaanggota4' => $request->nama_anggota4,
            'phone_number' => $request->phonenumber,
            'fotoanggota1' => $namafoto1,
            'fotoanggota2' => $namafoto2,
            'fotoanggota3' => $namafoto3,
            'fotoanggota4' => $namafoto4,
            'ktmanggota1' => $namaktm1,
            'ktmanggota2' => $namaktm2,
            'ktmanggota3' => $namaktm3,
            'ktmanggota4' => $namaktm4,
            'buktitransfer' => $namafilebayar
        ]);

        return redirect('/home');
    }

    public function update($id, Request $request)
    {

        $this->validate($request, [
            'namatim' => 'required',
            'kategorilomba' => 'required',
            'asalinstansi' => 'required',
            'alamatinstansi' => 'required',
            'nama_anggota1' => 'required',
            'phonenumber' => 'required|digits_between:10,13',
            'filektmanggota1' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota2' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota3' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota4' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota1' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota2' => 'required_with:filektmanggota2|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota3' => 'required_with:filektmanggota3|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota4' => 'required_with:filektmanggota4|file|image|mimes:jpeg,png,jpg|max:2048',
            'filebuktipembayaran' => 'file|image|mimes:jpeg,png,jpg|max:2048'
        ], [], [
            'namatim' => 'Nama Tim',
            'kategorilomba' => "Kategori Lomba",
            'asalinstansi' => 'Asal Instansi',
            'alamatinstansi' => "Alamat Instansi",
            'nama_anggota1' => "Nama Anggota 1",
            'phonenumber' => 'Phone Number',
            'filektmanggota1' => "File Kartu Pelajar / KTP Anggota 1",
            'filektmanggota2' => "File Kartu Pelajar / KTP Anggota 2",
            'filektmanggota3' => "File Kartu Pelajar / KTP Anggota 3",
            'filektmanggota4' => "File Kartu Pelajar / KTP Anggota 4",
            'filefotoanggota1' => "File Foto Anggota 1",
            'filefotoanggota2' => "File Foto Anggota 2",
            'filefotoanggota3' => "File Foto Anggota 3",
            'filefotoanggota4' => "File Foto Anggota 4",
            'filebuktipembayaran' => "File Bukti Pembayaran",
        ]);

        $baronas = baronas::find($id);

        $baronas->namatim = $request->namatim;
        $baronas->kategori = $request->kategorilomba;
        $baronas->asalinstansi = $request->asalinstansi;
        $baronas->alamatinstansi = $request->alamatinstansi;
        $baronas->namapembimbing = $request->nama_pembimbing;
        $baronas->namaanggota1 = $request->nama_anggota1;
        $baronas->namaanggota2 = $request->nama_anggota2;
        $baronas->namaanggota3 = $request->nama_anggota3;
        $baronas->namaanggota4 = $request->nama_anggota4;
        $baronas->phone_number = $request->phonenumber;

        if ($request->file('filebuktipembayaran') != "") {
            $buktiBayar = $request->file('filebuktipembayaran');
            $namafilebayar = $request->namatim . '-BAYAR-' . time() . '.' . $buktiBayar->getClientOriginalExtension();
            $buktiBayar->move(public_path() . '/baronas_file/buktibayar/', $namafilebayar);

            $baronas->buktitransfer = $namafilebayar;
        }

        if ($request->file('filektmanggota1') != "") {
            $ktm1 = $request->file('filektmanggota1');
            $namaktm1 = $request->namatim . '-KTM-' . time() . '.' . $ktm1->getClientOriginalExtension();
            $ktm1->move(public_path() . '/baronas_file/ktm_anggota1/', $namaktm1);

            $foto1 = $request->file('filefotoanggota1');
            $namafoto1 = $request->namatim . '-FOTO-' . time() . '.' . $foto1->getClientOriginalExtension();
            $foto1->move(public_path() . '/baronas_file/foto_anggota1/', $namafoto1);

            $baronas->fotoanggota1 = $namafoto1;
            $baronas->ktmanggota1 = $namaktm1;
        }

        if ($request->file('filektmanggota2') != "") {
            $ktm2 = $request->file('filektmanggota2');
            $namaktm2 = $request->namatim . '-KTM-' . time() . '.' . $ktm2->getClientOriginalExtension();
            $ktm2->move(public_path() . '/baronas_file/ktm_anggota2/', $namaktm2);

            $foto2 = $request->file('filefotoanggota2');
            $namafoto2 = $request->namatim . '-FOTO-' . time() . '.' . $foto2->getClientOriginalExtension();
            $foto2->move(public_path() . '/baronas_file/foto_anggota2/', $namafoto2);

            $baronas->fotoanggota2 = $namafoto2;
            $baronas->ktmanggota2 = $namaktm2;
        }

        if ($request->file('filektmanggota3') != "") {
            $ktm3 = $request->file('filektmanggota3');
            $namaktm3 = $request->namatim . '-KTM-' . time() . '.' . $ktm3->getClientOriginalExtension();
            $ktm3->move(public_path() . '/baronas_file/ktm_anggota3/', $namaktm3);

            $foto3 = $request->file('filefotoanggota3');
            $namafoto3 = $request->namatim . '-FOTO-' . time() . '.' . $foto3->getClientOriginalExtension();
            $foto3->move(public_path() . '/baronas_file/foto_anggota3/', $namafoto3);

            $baronas->fotoanggota3 = $namafoto3;
            $baronas->ktmanggota3 = $namaktm3;
        }

        if ($request->file('filektmanggota4') != "") {
            $ktm4 = $request->file('filektmanggota4');
            $namaktm4 = $request->namatim . '-KTM-' . time() . '.' . $ktm4->getClientOriginalExtension();
            $ktm4->move(public_path() . '/baronas_file/ktm_anggota4/', $namaktm4);

            $foto4 = $request->file('filefotoanggota4');
            $namafoto4 = $request->namatim . '-FOTO-' . time() . '.' . $foto4->getClientOriginalExtension();
            $foto4->move(public_path() . '/baronas_file/foto_anggota4/', $namafoto4);

            $baronas->fotoanggota4 = $namafoto4;
            $baronas->ktmanggota4 = $namaktm4;
        }

        $baronas->save();

        $admin = Auth::user()->is_admin;
        if ($admin) {
            return redirect('/admin/baronas');
        } else {
            return redirect('/home');
        }
    }

    public function downloadCsv()
    {
        $baronas = baronas::select('baronas.*', 'users.email')
            ->join('users', 'users.id', '=', 'baronas.users_id')
            ->get();

        $csv = new Export();
        $csv->build($baronas, [
            'nopendaftaran',
            'namatim',
            'kategori',
            'namaanggota1',
            'namaanggota2',
            'namaanggota3',
            'namaanggota4',
            'namapembimbing',
            'asalinstansi',
            'alamatinstansi',
            'phone_number',
            'email'
        ])->download('baronas' . date('Y-m-d') . '.csv');
    }

    public function adminindex()
    {
        $baronas = baronas::select('baronas.*', 'users.email')
            ->join('users', 'users.id', '=', 'baronas.users_id')
            ->get();

        $name = Auth::user()->name;
        return view('adminbaronas_dashboard', ['baronas' => $baronas, 'user' => $name]);
    }
    public function admintambah()
    {
        return view('baronas_admintambah');
    }
    public function adminstore(Request $request)
    {
        


        $this->validate($request, [
            'namatim' => 'required',
            'kategorilomba' => 'required',
            'asalinstansi' => 'required',
            'alamatinstansi' => 'required',
            'nama_anggota1' => 'required',
            'phonenumber' => 'required|digits_between:10,13',
            'filektmanggota1' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota2' => 'required_with:nama_anggota2|file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota3' => 'required_with:nama_anggota3|file|image|mimes:jpeg,png,jpg|max:2048',
            'filektmanggota4' => 'required_with:nama_anggota4|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota1' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota2' => 'required_with:filektmanggota2|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota3' => 'required_with:filektmanggota3|file|image|mimes:jpeg,png,jpg|max:2048',
            'filefotoanggota4' => 'required_with:filektmanggota4|file|image|mimes:jpeg,png,jpg|max:2048',
            'filebuktipembayaran' => 'required|file|image|mimes:jpeg,png,jpg|max:2048'
        ], [], [
            'namatim' => 'Nama Tim',
            'kategorilomba' => "Kategori Lomba",
            'asalinstansi' => 'Asal Instansi',
            'alamatinstansi' => "Alamat Instansi",
            'nama_anggota1' => "Nama Anggota 1",
            'nama_anggota2' => "Nama Anggota 2",
            'nama_anggota3' => "Nama Anggota 3",
            'nama_anggota4' => "Nama Anggota 4",
            'phonenumber' => 'Phone Number',
            'filektmanggota1' => "File Kartu Pelajar / KTP Anggota 1",
            'filektmanggota2' => "File Kartu Pelajar / KTP Anggota 2",
            'filektmanggota3' => "File Kartu Pelajar / KTP Anggota 3",
            'filektmanggota4' => "File Kartu Pelajar / KTP Anggota 4",
            'filefotoanggota1' => "File Foto Anggota 1",
            'filefotoanggota2' => "File Foto Anggota 2",
            'filefotoanggota3' => "File Foto Anggota 3",
            'filefotoanggota4' => "File Foto Anggota 4",
            'filebuktipembayaran' => "File Bukti Pembayaran",
        ]);


        $namafoto2 = '';
        $namafoto3 = '';
        $namafoto4 = '';
        $namaktm1 = '';
        $namaktm2 = '';
        $namaktm3 = '';
        $namaktm4 = '';

        if ($request->file('filebuktipembayaran') != "") {
            $buktiBayar = $request->file('filebuktipembayaran');
            $namafilebayar = $request->namatim . '-BAYAR-' . time() . '.' . $buktiBayar->getClientOriginalExtension();
            $buktiBayar->move(public_path() . '/baronas_file/buktibayar/', $namafilebayar);
        }

        if ($request->file('filektmanggota1') != "") {
            $ktm1 = $request->file('filektmanggota1');
            $namaktm1 = $request->namatim . '-KTM-' . time() . '.' . $ktm1->getClientOriginalExtension();
            $ktm1->move(public_path() . '/baronas_file/ktm_anggota1/', $namaktm1);

            $foto1 = $request->file('filefotoanggota1');
            $namafoto1 = $request->namatim . '-FOTO-' . time() . '.' . $foto1->getClientOriginalExtension();
            $foto1->move(public_path() . '/baronas_file/foto_anggota1/', $namafoto1);
        }

        if ($request->file('filektmanggota2') != "") {
            $ktm2 = $request->file('filektmanggota2');
            $namaktm2 = $request->namatim . '-KTM-' . time() . '.' . $ktm2->getClientOriginalExtension();
            $ktm2->move(public_path() . '/baronas_file/ktm_anggota2/', $namaktm2);

            $foto2 = $request->file('filefotoanggota2');
            $namafoto2 = $request->namatim . '-FOTO-' . time() . '.' . $foto2->getClientOriginalExtension();
            $foto2->move(public_path() . '/baronas_file/foto_anggota2/', $namafoto2);
        }

        if ($request->file('filektmanggota3') != "") {
            $ktm3 = $request->file('filektmanggota3');
            $namaktm3 = $request->namatim . '-KTM-' . time() . '.' . $ktm3->getClientOriginalExtension();
            $ktm3->move(public_path() . '/baronas_file/ktm_anggota3/', $namaktm3);

            $foto3 = $request->file('filefotoanggota3');
            $namafoto3 = $request->namatim . '-FOTO-' . time() . '.' . $foto3->getClientOriginalExtension();
            $foto3->move(public_path() . '/baronas_file/foto_anggota3/', $namafoto3);
        }

        if ($request->file('filektmanggota4') != "") {
            $ktm4 = $request->file('filektmanggota4');
            $namaktm4 = $request->namatim . '-KTM-' . time() . '.' . $ktm4->getClientOriginalExtension();
            $ktm4->move(public_path() . '/baronas_file/ktm_anggota4/', $namaktm4);

            $foto4 = $request->file('filefotoanggota4');
            $namafoto4 = $request->namatim . '-FOTO-' . time() . '.' . $foto4->getClientOriginalExtension();
            $foto4->move(public_path() . '/baronas_file/foto_anggota4/', $namafoto4);
        }
        //automatically create user
        $currentDate = new DateTime("now",new DateTimeZone('Asia/Jakarta'));

        $result = User::where('email', $request->emailketua)->get()->first();

        if (!isset($result->name)) {
            $user = new User;
            $user->name = $request->namatim;
            $user->email = $request->emailketua;
            $user->password = Hash::make('123456');
            $user->email_verified_at = $currentDate;
            $user->save();
        } else {
            $user = $result;
        }
        baronas::create([
            'users_id' => $userId = $user->id,
            'namatim' => $request->namatim,
            'kategori' => $request->kategorilomba,
            'asalinstansi' => $request->asalinstansi,
            'alamatinstansi' => $request->alamatinstansi,
            'namapembimbing' => $request->nama_pembimbing,
            'namaanggota1' => $request->nama_anggota1,
            'namaanggota2' => $request->nama_anggota2,
            'namaanggota3' => $request->nama_anggota3,
            'namaanggota4' => $request->nama_anggota4,
            'phone_number' => $request->phonenumber,
            'fotoanggota1' => $namafoto1,
            'fotoanggota2' => $namafoto2,
            'fotoanggota3' => $namafoto3,
            'fotoanggota4' => $namafoto4,
            'ktmanggota1' => $namaktm1,
            'ktmanggota2' => $namaktm2,
            'ktmanggota3' => $namaktm3,
            'ktmanggota4' => $namaktm4,
            'buktitransfer' => $namafilebayar
        ]);

        return redirect('/home');
    }
    public function adminedit($id)
    {
        $baronas = baronas::find($id)->get();
        $name = Auth::user()->name;
        return view('baronas_adminedit', ['baronas' => $baronas[0], 'user' => $name]);
    }

    public function adminpay($id, Request $request)
    {
        $filekwitansi = $request->file('kwitansi');

        $baronas = baronas::find($id);
        $baronas->nopendaftaran = $request->nomorpendaftaran;

        if ($filekwitansi) {

            $namaKwitansi = $request->namatim . '-KWITANSI-OLI0' . time() . '.' . $filekwitansi->getClientOriginalExtension();

            $filekwitansi->move(public_path() . '/baronas_file/kwitansi/', $namaKwitansi);
            $baronas->kwitansi = $namaKwitansi;
        }

        $baronas->save();
        return redirect('/admin/baronas');
    }

    public function admindelete($id)
    {
        $baronas = baronas::find($id);
        $baronas->delete();
        return redirect('/admin/baronas');
    }
}
