<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Hash;
use Laracsv\Export;
use auth;
use App\electra;

class ElectraController extends Controller
{

    public function downloadCsv() {
        $electra = electra::select('electras.*', 'users.email')
        ->join('users', 'users.id', '=', 'electras.users_id')
        ->get();

        $csv = new Export();
        $csv->build($electra, [
            'nopendaftaran',
            'namatim',
            'namaketua',
            'namaanggota',
            'tipependaftaran',
            'region',
            'asalsekolah',
            'phone_number',
            'email'
        ])->download('electra'.date('Y-m-d').'.csv');
    }

    public function adminindex()
    {
        $electra = electra::select('electras.*', 'users.email')
        ->join('users', 'users.id', '=', 'electras.users_id')
        ->get();
        return view('adminelectra_dashboard', ['electra' => $electra]);
    }
    public function admincari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table pegawai sesuai pencarian data
        $electra = DB::table('electras')
            ->where('namatim', 'like', "%" . $cari . "%")
            ->paginate(15);

        // mengirim data pegawai ke view index
        return view('adminelectra_dashboard', ['electra' => $electra]);
    }

    public function admintambah()
    {
        return view('electra_admintambah');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'namatim' => 'required',
            'namaketua' => 'required',
            'asalsekolah' => 'required',
            'phonenumber' => 'required',
            'filebuktipembayaran' => 'file|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $namafilebayar='';
        if ($request->file('filebuktipembayaran') != "") {
       
        $buktiBayar = $request->file('filebuktipembayaran');

        $namafilebayar = $request->namatim . '-BAYAR-OLI0' . time() . '.' . $buktiBayar->getClientOriginalExtension();

        $buktiBayar->move(public_path() . '/electra_file/buktibayar/', $namafilebayar);
        }
        electra::create([
            'users_id' => $userId = Auth::user()->id,
            'namatim' => $request->namatim,
            'namaketua' => $request->namaketua,
            'namaanggota' => $request->namaanggota,
            'asalsekolah' => $request->asalsekolah,
            'phone_number' => $request->phonenumber,
            'tipependaftaran' => $request->input('tipependaftaran'),
            'region' => $request->input('region'),
            'buktitransfer' => $namafilebayar
        ]);
        return redirect('/home');
    }
    public function adminstore(Request $request)
    {
        $this->validate($request, [
            'namatim' => 'required',
            'namaketua' => 'required',
            'asalsekolah' => 'required',
            'phonenumber' => 'required',
            'kwitansi' => 'file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $namafilebayar='';
        if ($request->file('filebuktipembayaran') != "") {
       
        $buktiBayar = $request->file('filebuktipembayaran');

        $namafilebayar = $request->namatim . '-BAYAR-OLI0' . time() . '.' . $buktiBayar->getClientOriginalExtension();

        $buktiBayar->move(public_path() . '/electra_file/buktibayar/', $namafilebayar);
        }
        $namaKwitansi='';
        if ($request->file('kwitansi') != "") {
       
        $filekwitansi = $request->file('kwitansi');

        $namaKwitansi = $request->namatim. '-KWITANSI-OLI0' . time() . '.' . $filekwitansi->getClientOriginalExtension();

        $filekwitansi->move(public_path() . '/electra_file/kwitansi/', $namaKwitansi);
        }
        //automatically create user
        $currentDate = new DateTime("now",new DateTimeZone('Asia/Jakarta'));

        $result = User::where('email', $request->emailketua)->get()->first();

        if (!isset($result->name)) {
            $user = new User;
            $user->name = $request->namatim;
            $user->email = $request->emailketua;
            $user->password = Hash::make('123456');
            $user->email_verified_at = $currentDate;
            $user->save();
        } else {
            $user = $result;
        }

        electra::create([
            'users_id' => $user->id,
            'namatim' => $request->namatim,
            'namaketua' => $request->namaketua,
            'namaanggota' => $request->namaanggota,
            'asalsekolah' => $request->asalsekolah,
            'phone_number' => $request->phonenumber,
            'tipependaftaran' => $request->input('tipependaftaran'),
            'region' => $request->input('region'),
            'buktitransfer' => $namafilebayar,
            'nopendaftaran' => $request->nopeserta,
            'kwitansi' => $namaKwitansi
        ]);

        return redirect('/admin/electra');
    }

    public function searchUser($email) {
        $result = User::where('email', $email)->get()->first();

        if (isset($result->name)) {
            return true;
        }

        else {
            return false;
        }
    }

    public function edit()
    {
        $id = Auth::user()->id;

        $electra = DB::table('electras')->where('users_id', $id)->get();
        if ($electra->first()) {
            //$electra = electra::find($id);
            return view('electra_edit', ['electra' => $electra]);
        } else {
            // return view('electra_tambah' );
            return redirect()->back()->withErrors(["failed" => "Pendaftaran Sudah Ditutup"]);
        }
        //<?php echo $id 
    }
    public function adminedit($id)
    {
        $electra = electra::find($id);
        return view('electra_adminedit', ['electra' => $electra ]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'namatim' => 'required',
            'namaketua' => 'required',
            'namaanggota' => 'required',
            'asalsekolah' => 'required',
            'phonenumber' => 'required',
            'filebuktipembayaran' => 'file|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $electra = electra::find($id);
        $electra->namatim = $request->namatim;
        $electra->namaketua = $request->namaketua;
        $electra->namaanggota = $request->namaanggota;
        $electra->asalsekolah = $request->asalsekolah;
        $electra->phone_number = $request->phonenumber;
        $electra->tipependaftaran = $request->input('tipependaftaran');
        $electra->region = $request->input('region');
        
        if ($request->file('filebuktipembayaran') != "") {
            $buktiBayar = $request->file('filebuktipembayaran');

            $namafilebayar = $request->namatim . '-BAYAR-OLI0' . time() . '.' . $buktiBayar->getClientOriginalExtension();

            $buktiBayar->move(public_path() . '/electra_file/buktibayar/', $namafilebayar);

            $electra->buktitransfer = $namafilebayar;
        }
        $electra->save();

        $admin = Auth::user()->is_admin;
        if($admin)  {
            return redirect('/admin/electra');
        } else {
            return redirect('/home');
        }
    }

    public function adminupdate($id, Request $request)
    {
        $this->validate($request, [
            'namatim' => 'required',
            'namaketua' => 'required',
            'namaanggota' => 'required',
            'asalsekolah' => 'required',
            'phonenumber' => 'required'

        ]);

        $electra = electra::find($id);
        $electra->namatim = $request->namatim;
        $electra->namaketua = $request->namaketua;
        $electra->namaanggota = $request->namaanggota;
        $electra->asalsekolah = $request->asalsekolah;
        $electra->phone_number = $request->phonenumber;
        $electra->tipependaftaran = $request->input('tipependaftaran');
        $electra->region = $request->input('region');
        
        if ($request->file('filebuktipembayaran') != "") {
            $buktiBayar = $request->file('filebuktipembayaran');

            $namafilebayar = $request->namatim . '-BAYAR-OLI0' . time() . '.' . $buktiBayar->getClientOriginalExtension();

            $buktiBayar->move(public_path() . '/electra_file/buktibayar/', $namafilebayar);

            $electra->buktitransfer = $namafilebayar;
        }
        $electra->save();
        return redirect('/admin/electra');
    }

    public function admindelete($id)
    {
        $electra = electra::find($id);
        $electra->delete();
        return redirect('/admin/electra');
    }

    public function adminpay($id, Request $request)
    {
        $this->validate($request, [
            'nomorpendaftaran' => 'required',
            'kwitansi' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $filekwitansi = $request->file('kwitansi');

        $namaKwitansi = $request->namatim. '-KWITANSI-OLI0' . time() . '.' . $filekwitansi->getClientOriginalExtension();

        $filekwitansi->move(public_path() . '/electra_file/kwitansi/', $namaKwitansi);

        $electra = electra::find($id);
        $electra->nopendaftaran = $request->nomorpendaftaran;
        $electra->kwitansi = $namaKwitansi;

        $electra->save();
        return redirect('/admin/electra');
    }
}
