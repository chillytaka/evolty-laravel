<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KwitansiController extends Controller
{
    //
    public function createKwitansi()
    {
        //Set the Content Type
        header('Content-type: image/jpeg');

        // Create Image From Existing File
        $jpg_image = imagecreatefromjpeg(public_path() . '/image/kwitansi.jpeg');

        $black = imagecolorallocate($jpg_image, 0, 0, 0);

        // Set Path to Font File
        $font_path = public_path() . '/font/font coolvetica/coolvetica-rg.ttf';

        // Set Text to Be Printed On Image
        $terimaDari = "Electra 2020";
        $terbilang = 'Seratus dua puluh ribu';
        $jumlah = "120.000,00,-";

        // Print Text On Image
        imagettftext($jpg_image, 15, 0, 300, 200, $black, $font_path, $terimaDari);
        imagettftext($jpg_image, 12, 0, 300, 215, $black, $font_path, $terbilang);
        imagettftext($jpg_image, 12, 0, 310, 235, $black, $font_path, $terimaDari);
        imagettftext($jpg_image, 18, 0, 220, 290, $black, $font_path, $jumlah);

        // Send Image to Browser
        imagejpeg($jpg_image);

        // Clear Memory
        imagedestroy($jpg_image);
    }
}
