<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\news;

class NewsController extends Controller
{
    public function index() {
        $news = news::orderBy('updated_at', 'asc')->get();
        return  view('news', ['news' => $news]);
    }

    public function contentShow($id) {
        $news = news::find($id);

        $content = Storage::disk('local')->get('news_file/posts/' . $news->path_content);
        return  view('content_news', ['news' => $news, 'content' => $content]);
    }
    public function adminindex() {
        $news = news::get();
        return view('adminnews_dashboard', ['news' => $news]);
    }

    public function admindelete($id)
    {
        $news = news::find($id);
        $news->delete();
        return redirect('/admin/news');
    }

    public function adminedit($id)
    {
        $news = news::find($id);

        $content = Storage::disk('local')->get('news_file/posts/' . $news->path_content);
        return view('news_adminedit', ['news' => $news, 'content' => $content]);
    }

    public function update($id, Request $request) 
    {
        $news = news::find($id);
        $news->title = $request->news_title;

        $news_content = $request->news_content;
        $news_header = $request->file('news_headline_img');

        $file_name = $news->path_content;

        if($news_header != ""){
            $header_name = $request->news_title . '-' . time() . '.' . $news_header->getClientOriginalExtension();
            $news_header->move(public_path() . '/news_file/headline/', $header_name);
            $news->path_headline = $header_name;
        }

        //saving news_content to files
        Storage::disk('local')->put('news_file/posts/'. $file_name, $news_content);

        $news->save();

        return redirect()->back();
    }

    //storing text to database
    public function store(Request $request) {
        $news_content = $request->news_content;
        $news_header = $request->file('news_headline_img');

        $file_name = $request->news_title . '-' . time() . '.txt';
        $header_name = '';

        if($news_header != ""){
            $header_name = $request->news_title . '-' . time() . '.' . $news_header->getClientOriginalExtension();
            $news_header->move(public_path() . '/news_file/headline/', $header_name);
        }

        //saving news_content to files
        Storage::disk('local')->put('news_file/posts/'. $file_name, $news_content);

        news::create([
            'title' => $request->news_title,
            'path_headline' => $header_name,
            'path_content' => $file_name,
        ]);

        return redirect()->back();
    }

    // upload files from dialogue
    public function uploader(Request $request) {

        $file = $request->file('upload');
        $file_name = Str::random(10) . time() . '.'. $file->getClientOriginalName();
        $file->move(public_path(). '/news_file/', $file_name);

        $CKEditorFuncNum = $request->CKEditorFuncNum;
        $msg = "File Uploaded Successfully";
        $url = '/news_file/' . $file_name;

        $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

        @header('Content-type: text/html; charset=utf-8');
        echo $response;
    }

    // upload files using drag + drop 
    public function uploaderJson(Request $request) {

        $file = $request->file('upload');
        $file_name = Str::random(10) . time() . '.'. $file->getClientOriginalName();
        $file->move(public_path(). '/news_file/', $file_name);

        $CKEditorFuncNum = $request->CKEditorFuncNum;
        $msg = "File Uploaded Successfully";
        $url = '/news_file/' . $file_name;

        return response()->json(['uploaded' => 1, 'fileName'=> $file_name, 'url' => $url]);
    }
}
