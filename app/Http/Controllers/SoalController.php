<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth;
use App\Olimpiade;
use App\pesertaolim;
use App\electra;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class SoalController extends Controller
{
    public function index()
    {
    	$soal = Olimpiade::all();
    	return view('soal', ['soal' => $soal]);
    }
    public function tambah()
    {
        return view('soaltambah');
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'soal' => 'required',
            'pilihan1' => 'required',
            'pilihan2' => 'required',
            'pilihan3' => 'required',
            'pilihan4' => 'required',
            'pilihan5' => 'required',
            
            'gambar' => 'file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $namagambar='';
        if ($request->file('gambar') != "") {
       
        $gambaran = $request->file('gambar');

        $namagambar = $request->namatim . '-Gambar-OLI0' . time() . '.' . $gambaran->getClientOriginalExtension();

        $gambaran->move(public_path() . '/olimpiade/gambar/', $namagambar);
        }
        
       
        Olimpiade::create([
            
            'soal' => $request->soal,
            'pilihan1' => $request->pilihan1,
            'pilihan2' => $request->pilihan2,
            'pilihan3' => $request->pilihan3,
            'pilihan4' => $request->pilihan4,
            'pilihan5' => $request->pilihan5,
            'jawaban' => $request->pilihan5,
            
            'tipesoal' => $request->input('tipesoal'),
            'gambar' => $namagambar
        ]);

        return redirect('/admin/soal');
    }
    public function edit($id)
    {
        $soal = Olimpiade::find($id);
        return view('soal_adminedit', ['soal' => $soal ]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'soal' => 'required',
            'pilihan1' => 'required',
            'pilihan2' => 'required',
            'pilihan3' => 'required',
            'pilihan4' => 'required',
            'pilihan5' => 'required',
            
            'gambar' => 'file|image|mimes:jpeg,png,jpg|max:2048',
        
        ]);

        $soal = Olimpiade::find($id);
        $soal->soal = $request->soal;
        $soal->pilihan1 = $request->pilihan1;
        $soal->pilihan2 = $request->pilihan2;
        $soal->pilihan3 = $request->pilihan3;
        $soal->pilihan4 = $request->pilihan4;
        $soal->tipesoal = $request->input('tipesoal');
        
        if ($request->file('gambar') != "") {
            $gambaran = $request->file('gambar');

            $namagambar = $request->namatim . '-BAYAR-OLI0' . time() . '.' . $gambaran->getClientOriginalExtension();

            $gambaran->move(public_path() . '/olimpiade/gambar/', $namagambar);

            $soal->gambar = $namagambar;
        }
        $soal->save();
        return redirect('/admin/soal');
    }
    public function delete($id)
    {
        $soal = Olimpiade::find($id);
        $soal->delete();
        return redirect('/admin/soal');
    }
    public function random()
    {
        $user = electra::all();
        
        foreach($user as $u)
            {
                echo $u->users_id;
                $soal= Olimpiade::inRandomOrder()->limit(3)->get();
                foreach($soal as $s)
                    {
                        echo $s->id;
                        //masukkin data ke table userAnswer
                        pesertaolim::create([
            
                            'users_id' => $u->users_id,
                            'soalid' => $s->id,
                            'jawaban' => '',
                            'status' => ''
                            
                        ]);
                        
                    }
            }
    }


}
