<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Olimpiade extends Model
{
    protected $table = "olimpiades";
    protected $fillable = ['gambar','soal', 'pilihan1','pilihan2','pilihan3','pilihan4'
    ,'pilihan5','jawaban','tipesoal'];
}
