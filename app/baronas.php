<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class baronas extends Model
{
    protected $table = "baronas";
    public function users()
    {
    	return $this->belongsTo('App\User');
    }
    protected $fillable = [
        'users_id', 
        'namatim',
        'kategori',
        'namaanggota1',
        'namaanggota2',
        'namaanggota3',
        'namaanggota4',
        'fotoanggota1',
        'fotoanggota2',
        'fotoanggota3',
        'fotoanggota4',
        'ktmanggota1',
        'ktmanggota2',
        'ktmanggota3',
        'ktmanggota4',
        'namaanggota',
        'namapembimbing',
        'asalinstansi',
        'alamatinstansi',
        'phone_number',
        'buktitransfer',
        'nopendaftaran',
        'kwitansi'];
}
