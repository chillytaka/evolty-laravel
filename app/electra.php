<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class electra extends Model
{
    protected $table = "electras";
    public function users()
    {
    	return $this->belongsTo('App\User');
    }
    protected $fillable = ['namatim','namaketua','users_id',
    'namaanggota','region','tipependaftaran','asalsekolah'
    ,'phone_number','buktitransfer',
    'nopendaftaran','kwitansi'];
}
