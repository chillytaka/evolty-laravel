<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pesertaolim extends Model
{
    protected $table = "pesertaolims";
    protected $fillable = ['users_id','soalid', 'jawaban','status'];

}
