<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('users_id');
            $table->string('namatim',20);
            $table->string('namaketua',40);
            $table->string('namaanggota',40)->nullable();
            $table->string('tipependaftaran',10);
            $table->string('region',20);
            $table->string('asalsekolah',40);
            $table->string('buktitransfer')->nullable();
            $table->string('kwitansi')->nullable();
            $table->string('phone_number', 100);
            $table->string('nopendaftaran',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electras');
    }
}
