<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaronasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baronas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('users_id');
            $table->string('namatim',20);
            $table->string('kategori',40);

            $table->string('namaanggota1',40)->nullable();
            $table->string('namaanggota2',40)->nullable();
            $table->string('namaanggota3',40)->nullable();
            $table->string('namaanggota4',40)->nullable();

            $table->string('fotoanggota1')->nullable();
            $table->string('fotoanggota2')->nullable();
            $table->string('fotoanggota3')->nullable();
            $table->string('fotoanggota4')->nullable();

            $table->string('ktmanggota1')->nullable();
            $table->string('ktmanggota2')->nullable();
            $table->string('ktmanggota3')->nullable();
            $table->string('ktmanggota4')->nullable();

            $table->string('namapembimbing',40)->nullable();

            $table->string('asalinstansi');
            $table->string('alamatinstansi');

            $table->string('buktitransfer');
            $table->string('kwitansi')->nullable();
            $table->string('phone_number', 100);
            $table->string('nopendaftaran',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baronas');
    }
}
