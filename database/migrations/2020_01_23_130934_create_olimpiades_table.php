<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOlimpiadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olimpiades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('soal');
            $table->string('gambar');
            $table->string('pilihan1',250)->nullable();
            $table->string('pilihan2',250)->nullable();
            $table->string('pilihan3',250)->nullable();
            $table->string('pilihan4',250)->nullable();
            $table->string('pilihan5',250)->nullable();
            $table->string('tipesoal',50)->nullable();
            $table->string('jawaban',250)->nullable();
            
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olimpiades');
    }
}
