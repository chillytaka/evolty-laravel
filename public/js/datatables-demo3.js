// waiting for document to be ready
$(document).ready(function() {
    // Call the dataTables jQuery plugin
    $("#dataTable").DataTable({
        scrollX: true,
        order: [[8, "asc"]]
    });

    $("#newsTable").DataTable({
        scrollX: true
    });
});

$("select[name=kategorilomba]").change(function() {
    console.log($(this).val);
    if ($(this).val() == "Umum - ROV") {
        $("#anggota4").prop("disabled", false);
    } else {
        $("#anggota4").prop("disabled", true);
    }
});
