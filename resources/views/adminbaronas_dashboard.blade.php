@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Peserta Baronas</h1>
</div>

<button class="btn btn-primary mb-2" onclick="location.href='/admin/baronas/csv'">Download Data</button>

<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>Asal Instansi</th>
                <th>Alamat Instansi</th>
                <th>Nama Tim</th>
                <th>Nama Pembimbing</th>
                <th>Nama Anggota</th>
                <th>Kategori Lomba</th>
                <th>No handphone</th>
                <th>Photo</th>
                <th>Waktu Update</th>
                <th>Email</th>
                <th>Nomor Pendaftaran</th>
                <th>OPSI</th>
            </tr>
        </thead>
        <tbody>
            @foreach($baronas as $e)
            <tr>
                <td>{{ $e->asalinstansi}}</td>
                <td>{{$e->alamatinstansi}}</td>
                <td>{{ $e->namatim }}</td>
                <td>{{$e->namapembimbing}}</td>
                <td>
                    <div>{{ $e->namaanggota1 }}</div>
                    @if ($e->namanaggota2)
                    <div>{{ $e->namaanggota2 }}</div>
                    @endif

                    @if ($e->namanaggota3)
                    <div>{{ $e->namaanggota3 }}</div>
                    @endif

                    @if ($e->namanaggota4)
                    <div>{{ $e->namaanggota4 }}</div>
                    @endif
                </td>
                <td>{{ $e->kategori }}</td>
                <td>{{ $e->phone_number }}</td>

                <td>
                    @if ($e->ktmanggota1)
                    <div>anggota 1</div>
                    <a href="{{url( '/baronas_file/ktm_anggota1/'. $e->ktmanggota1)}}">{{$e->ktmanggota1}}</a><br>
                    <a href="{{url( '/baronas_file/foto_anggota1/'. $e->fotoanggota1)}}">{{$e->fotoanggota1}}</a><br>
                    @endif

                    @if ($e->ktmanggota2)
                    <div>anggota 2</div>
                    <a href="{{url( '/baronas_file/ktm_anggota2/'. $e->ktmanggota2)}}">{{$e->ktmanggota2}}</a><br>
                    <a href="{{url( '/baronas_file/foto_anggota2/'. $e->fotoanggota2)}}">{{$e->fotoanggota2}}</a><br>
                    @endif

                    @if ($e->ktmanggota3)
                    <div>anggota 3</div>
                    <a href="{{url( '/baronas_file/ktm_anggota3/'. $e->ktmanggota3)}}">{{$e->ktmanggota3}}</a><br>
                    <a href="{{url( '/baronas_file/foto_anggota3/'. $e->fotoanggota3)}}">{{$e->fotoanggota3}}</a><br>
                    @endif

                    @if ($e->ktmanggota4)
                    <div>anggota 4</div>
                    <a href="{{url( '/baronas_file/ktm_anggota4/'. $e->ktmanggota4)}}">{{$e->ktmanggota4}}</a><br>
                    <a href="{{url( '/baronas_file/foto_anggota4/'. $e->fotoanggota4)}}">{{$e->fotoanggota4}}</a><br>
                    @endif

                    @if ($e->buktitransfer)
                    <div>bukti bayar</div>
                    <a href="{{url( '/baronas_file/buktibayar/'. $e->buktitransfer)}}">{{$e->buktitransfer}}</a>
                    @endif
                </td>

                <td>{{$e->updated_at}}</td>
                <td>{{$e->email}}</td>

                @if($e->nopendaftaran)
                <td>
                    <div>{{$e->nopendaftaran}}</div>
                    <a href="{{url( '/baronas_file/kwitansi/'. $e->kwitansi)}}">{{$e->kwitansi}}</a>
                </td>
                @else
                <td>Belum Terverifikasi</td>
                @endif


                <td>

                    <a href="/admin/baronas/edit/{{ $e->id }}" class="btn btn-warning">Edit</a>
                    <a href="/admin/baronas/hapus/{{ $e->id }}" class="btn btn-danger">Hapus</a>
                    <button type="button" class="btn btn-success" data-toggle="modal"
                        data-target="#myModal{{$e->id}}">Verifikasi</button>

                </td>

                <!-- Modal -->
                <div class="modal fade" id="myModal{{$e->id}}" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"> VERIFIKASI</h4>

                            </div>
                            <div class="modal-body">
                                <p>ISI NOMOR PENDAFTARAN</p>

                                <form method="post" action="/admin/baronas/bayar/{{ $e->id }}"
                                    enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <input name="namatim" type="hidden" value="{{$e->namatim}}" />

                                    <div class="form-group">
                                        <label>Nomor Pendaftaran</label>
                                        <textarea name="nomorpendaftaran" class="form-control"
                                            placeholder=" No pendaftaran .."> {{ $e->nopendaftaran }} </textarea>

                                        @if($errors->has('nomorpendaftaran'))
                                        <div class="text-danger">
                                            {{ $errors->first('nomorpendaftaran')}}
                                        </div>
                                        @endif


                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <label>Kwitansi</label>
                                                <input type="file" class="form-control-file" name="kwitansi">

                                            </div>

                                            @if($errors->has('kwitansi'))
                                            <div class="text-danger">
                                                {{ $errors->first('kwitansi')}}
                                            </div>
                                            @endif

                                        </div>


                                        <div class="form-group">
                                            <input type="submit" class="btn btn-success" value="Simpan">
                                        </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection