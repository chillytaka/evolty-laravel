@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Peserta Electra</h1>
</div>

<button class="btn btn-primary mb-2" onclick="location.href='/admin/electra/csv'">Download Data</button>

<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>Nama Tim</th>
                <th>Nama Ketua</th>
                <th>Nama Anggota</th>
                <th>Tipe Pendaftaran</th>
                <th>Region</th>
                <th>No handphone</th>
                <th>Email</th>
                <th>Photo</th>
                <th>Waktu Update</th>
                <th>Nomor Pendaftaran</th>
                <th>OPSI</th>
            </tr>
        </thead>
        <tbody>
            @foreach($electra as $e)
            <tr>
                <td>{{ $e->namatim }}</td>
                <td>{{ $e->namaketua }}</td>
                <td>{{ $e->namaanggota }}</td>
                <td>{{ $e->tipependaftaran }}</td>
                <td>{{ $e->region }}</td>
                <td>{{ $e->phone_number }}</td>
                <td>{{$e->email}}</td>

                <td>
                    @if ($e->buktitransfer)
                    <a href="{{url( '/electra_file/buktibayar/'. $e->buktitransfer)}}">{{$e->buktitransfer}}</a>
                    @endif
                </td>

                <td>{{$e->updated_at}}</td>

                @if($e->nopendaftaran)
                <td>{{$e->nopendaftaran}}</td>
                @else
                <td>Belum Terverifikasi</td>
                @endif

                <td>

                    <a href="/admin/electra/edit/{{ $e->id }}" class="btn btn-warning">Edit</a>
                    <a href="/admin/electra/hapus/{{ $e->id }}" class="btn btn-danger">Hapus</a>
                    <button type="button" class="btn btn-success" data-toggle="modal"
                        data-target="#myModal{{$e->id}}">Verifikasi</button>

                </td>

                <!-- Modal -->
                <div class="modal fade" id="myModal{{$e->id}}" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"> VERIFIKASI</h4>

                            </div>
                            <div class="modal-body">
                                <p>ISI NOMOR PENDAFTARAN</p>

                                <form method="post" action="/admin/electra/bayar/{{ $e->id }}"
                                    enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <input name="namatim" type="hidden" value="{{$e->namatim}}" />

                                    <div class="form-group">
                                        <label>Nomor Pendaftaran</label>
                                        <textarea name="nomorpendaftaran" class="form-control"
                                            placeholder=" No pendaftaran .."> {{ $e->nopendaftaran }} </textarea>

                                        @if($errors->has('nomorpendaftaran'))
                                        <div class="text-danger">
                                            {{ $errors->first('nomorpendaftaran')}}
                                        </div>
                                        @endif


                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <label>Kwitansi</label>
                                                <input type="file" class="form-control-file" name="kwitansi">

                                            </div>

                                            @if($errors->has('kwitansi'))
                                            <div class="text-danger">
                                                {{ $errors->first('kwitansi')}}
                                            </div>
                                            @endif

                                        </div>


                                        <div class="form-group">
                                            <input type="submit" class="btn btn-success" value="Simpan">
                                        </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection