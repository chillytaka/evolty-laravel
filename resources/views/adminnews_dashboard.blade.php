@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List News</h1>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="newsTable">
        <thead>
            <tr>
                <th>Title</th>
                <th>Path Headline</th>
                <th>Waktu Diupdate</th>
                <th>Waktu Diunggah</th>
                <th>OPSI</th>
            </tr>
        </thead>
        <tbody>
            @foreach($news as $e)
            <tr>
                <td>{{ $e->title }}</td>

                <td>
                    @if ($e->path_headline)
                    <a href="{{url( '/news_file/headline/'. $e->path_headline)}}">{{$e->path_headline}}</a>
                    @endif
                </td>

                <td>{{$e->updated_at}}</td>
                <td>{{$e->created_at}}</td>

                <td>
                    <a href="/admin/news/edit/{{ $e->id }}" class="btn btn-warning">Edit</a>
                    <a href="/admin/news/hapus/{{ $e->id }}" class="btn btn-danger">Hapus</a>
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection