<html>

<head>
    <meta charset="utf-8">
    <title>Login | EVOLTY</title>
    <link rel="shorcut icon" href="{{asset('image/evolty.png') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script src='https://ricostacruz.com/jquery.transit/jquery.transit.min.js'></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="css/style_s.css">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
</head>

<body>

    @include('partials.navbar')

    <div class="section">

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div id="particles"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-6 sm-12 isi-box animated fadeInLeft" id="logo-login">
                        <img src="image/evolty.png" id="logomage">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 isi-box animated fadeInRight" id="isi-login">
                        <span class="form-judul">Login</span>

                        {{-- ERROR REPORT --}}
                        @error('email')
                        <span class="txt-error">{{$message}}</span><br>
                        @enderror

                        @error('password')
                        <span class="txt-error">{{$message}}</span><br>
                        @enderror

                        {{-- INPUT FORM --}}
                        <div class="form-masukan">
                            <input class="masukan" type="text" name="email" placeholder="E-mail" required
                                autocomplete="off" id="email" value="{{ old('email')}} " autofocus>
                            <span class="symbol-masukan">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="form-masukan">
                            <input class="masukan" id="showPassword" type="password" name="password" required
                                placeholder="Password" style="padding-right:68px;">
                            <span class="symbol-masukan">
                                <i class="fa fa-lock" aria-hidden="lock"></i>
                            </span>
                        </div>
                        <div class="show-password"
                            style=" text-align: left; padding-left: 35px; font-size: 0.75em; display: flex; align-items: center;">
                            <input type="checkbox" onclick="showPassword()"><span style="margin-left: 20px;">Show
                                Password</span>
                        </div>
                        <br>

                        <button class="btn-selengkap" style="background-color:#4971AA; background-size: cover; "
                            type="submit">Login</button>

                        <div class=" text_daftar">
                            <span class="txt1">
                                Don't have an account?
                            </span>
                            <a class="txt2" href="/register">
                                sign up here
                            </a>
                        </div>

                        <div class=" text_daftar">
                            <a class="txt2" href="/password/reset">
                                Forgot Password ?
                            </a>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    <script src="{{asset('js/restrict.js') }}"></script>
    <script type="text/javascript" src="js/navbar2.js"></script>

</body>