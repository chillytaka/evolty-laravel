<html>

<head>
    <meta charset="utf-8">
    <title>Registration | EVOLTY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shorcut icon" href="{{asset('image/evolty.png') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

    <script src='https://ricostacruz.com/jquery.transit/jquery.transit.min.js'></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="css/style_s.css">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
</head>

<body>

    @include('partials.navbar')

    <div class="section">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div id="particles"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 isi-box animated fadeInLeft" id="logo-login">
                        <img src="{{asset('image/evolty.png') }}" id="logomage">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 isi-box animated fadeInRight" id="isi-login">
                        <span class="form-judul">Account registration</span>

                        {{-- ERROR REPORT --}}
                        @error('email')
                        <span class="txt-error">{{$message}}</span><br>
                        @enderror
                        @error('password')
                        <span class="txt-error">{{$message}}</span><br>
                        @enderror
                        @error('name')
                        <span class="txt-error">{{$message}}</span><br>
                        @enderror

                        {{-- INPUT FORM --}}
                        <div class="form-masukan">
                            <input class="masukan" type="text" name="name" id="name" value="{{old('name') }}" required
                                autocomplete="name" placeholder="Username">
                            <span class="symbol-masukan">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="form-masukan">
                            <input class="masukan" type="email" name="email" placeholder="E-mail" required
                                autocomplete="email" id="email" value="{{ old('email') }}">
                            <span class="symbol-masukan">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>


                        <div class="form-masukan">
                            <input class="masukan" type="password" id="password" name="password" required
                                placeholder="Password" autocomplete="new-password">
                            <span class="symbol-masukan">
                                <i class="fa fa-lock" aria-hidden="lock"></i>
                            </span>
                        </div>

                        <div class="form-masukan">
                            <input class="masukan" type="password" name="password_confirmation" required
                                placeholder="Confirm Password" id="password-confirm" autocomplete="new-password">
                            <span class="symbol-masukan">
                                <i class="fa fa-key" aria-hidden="lock"></i>
                            </span>
                        </div>

                        <input type="submit" class="btn-selengkap"
                            style="background-color:#4971AA; background-size: cover; " value="SUBMIT">

                        <div class="text_daftar">
                            <span class="txt1">
                                Already have an account?
                            </span>
                            <a class="txt2" href="/login">
                                Login
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript" src="js/navbar2.js"></script>
</body>

</html>