<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shorcut icon" href="image/evolty.png">
    <title>Welcome | Baronas 2020</title>
    <link rel="stylesheet" href="css/style_s.css">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
</head>

<body style="background-image:url('image/BARONAS/BACKGROUND BARONAS.png');">

    <div id="overlay-2">
        <div id="popup-2">
            <div id="close-2">X</div>
            <div class="container popke1">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>We've already updated the rulebook</h2>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        
                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">                                
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Rulebook SD
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSD'">Read More</div>
                                </div>
                            </div>
                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Rulebook SMP                                   
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSMP'">Read More</div>
                                </div>
                            </div>

                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Rulebook SMA                                   
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSMA'">Read More</div>
                                </div>
                            </div>
                            
                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Sumo                               
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSumo-EN'">Read More</div>
                                </div>
                            </div>                                                  
                            {{-- <br>
                            Rulebook SMP
                            <br>
                            Rulebook SMA
                            <br>
                            Sumo --}}
                        
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="background" style="">

        @include('partials.navbar')

        <section class="section1" style="margin-top: 100px;">
            <div class="container">
                <div class="row">
                    <div class="wrapper-flex-center d-flex justify-content-center">
                        <img id="logo-evolty" src="image/BARONAS/BARONAS.png" alt="" srcset="">
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="section2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper-flex-center" style="justify-content: center; align-items: center;">
                        <img src="image/tulisan-05.png" alt="" srcset="" style="width: 20%;">
                        <div class="tulisan-1"
                            style="font-size: 6vw; font-family: product_sans; font-weight: bold; letter-spacing: 7.5px;">
                            BARONAS
                        </div>
                        <img src="image/tulisan-05.png" alt="" srcset="" style="width: 20%; transform: rotate(180deg);">
                    </div>
                </div>
            </div>
            </!DOCTYPE>
    </section>

    <div class="background" style="">
        <section class="section3" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="width: auto;">
                                <div class="tulisan-2-judul">
                                    WHAT IS BARONAS?
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="">
                                <span style="">It's a robotics competition that aims to
                                    introduce and hone the abilities of students and the general public
                                    in Indonesia in the field of Robotics Technology.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section4" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    THEME
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="text-align:center;">
                                <span style="">"Marine Technology Development" </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div id="judul-sub-event" class="tulisan-2-judul">
                                    TOTAL PRIZE
                                </div>
                            </div>
                            <div class="tulisan-3-kontenelectrath wrapper-flex-center d-flex justify-content-center">
                                <span style="font-size:9vw;">Rp 35.000.000</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section6" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    CATEGORIES
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.1 SD.png" alt="" srcset="">
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Line Tracer Analog
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (Elementary School)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            The initial position of the robot is at the Dock (start) carrying objects which
                            are then carried through the "BARO" path to the Dock as a place to load cargo carried
                            by the captain. The robot will bring the object to the pier back through the "NAS" path.
                        </div>
                        <br>
                        <a href="/rbSD">
                            <button class="buttonKategoriBaronas">
                                Read More
                            </button>
                        </a>

                        <a href="/trSD">
                            <button class="buttonKategoriBaronas">
                                Download Track
                            </button>
                        </a>

                        <a href="/boxSD">
                            <button class="buttonKategoriBaronas">
                                Download Box
                            </button>
                        </a>
                    </div>
                </div>
                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.2 SMP.png" alt="" srcset="">
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Line Tracer Analog and Transporter
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (Junior High School)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            The challenges given in accordance with the theme are participants must
                            make robots that can do several missions which include
                            take-up buffer, crown and head beam. Then The buffer is installed in its
                            place and the last place head beam and crown on the Poseidon Throne.
                            Variation of obstacles the track will also be a separate test for transporter robots.
                        </div>
                        <br>
                        <a href="/rbSMP">
                            <button class="buttonKategoriBaronas">
                                Read More
                            </button>
                        </a>

                        <a href="/trSMP">
                            <button class="buttonKategoriBaronas">
                                Download Track
                            </button>
                        </a>
                        <a href="/boxSMP">
                            <button class="buttonKategoriBaronas">
                                Download Box
                            </button>
                        </a>
                        <a href="/objSMP">
                            <button class="buttonKategoriBaronas">
                                Download Object Lomba
                            </button>
                        </a>
                    </div>
                </div>

                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.3 SMA.png" alt="" srcset="">
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Line Tracer Micro and Transporter
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (High School)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            The challenges given in accordance with the theme are
                            participants must direct the two robots that must work together
                            to build a treasure chest of goods
                            which has been provided (2 beams, 1 tube) in sequence from
                            the most basic is the gold-rushed beam, then
                            followed by a block above the diamond block and finally
                            the tube is called the crown at the top.
                        </div>
                        <br>
                        <a href="/rbSMA">
                            <button class="buttonKategoriBaronas">
                                Read More
                            </button>
                        </a>
                        <a href="/trSMA">
                            <button class="buttonKategoriBaronas">
                                Download Track
                            </button>
                        </a>
                        <a href="/objSMA">
                            <button class="buttonKategoriBaronas">
                                Download Object Lomba
                            </button>
                        </a>
                    </div>
                </div>

                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.4 SUMO.png" alt="" srcset="">
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Sumo
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (Open Category)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            Participants make a sumo robot that can push the opposing robot out of the arena in
                            accordance with the rules of the match. The robot operator controls the sumo robot
                            by using remote wireless.
                        </div>
                        <br>
                        <a href="/rbSumo-EN">
                            <button class="buttonKategoriBaronas">
                                Read More
                            </button>
                        </a>
                    </div>
                </div>

                <br>
                <br>

                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.5 UNDERWATER ROBOT.png" alt="" srcset="">
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Coming Soon
                            {{-- Underwater ROV --}}
                        </div>
                        {{--                         <div class="pesertaKategoriBaronas" style="">
                            (Open Category)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            Underwater Robotic Competition (URC) merupakan robot bawah laut
                            yang dikendalikan jarak jauh dengan operator yang berada pada jarak
                            aman.
                        </div>
                        <br>
                        <a href="/rbUnderwater-EN">
                            <button class="buttonKategoriBaronas">
                                Selengkapnya
                            </button>
                        </a> --}}
                    </div>
                </div>

                <br>
                <br>

            </div>

            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center; ">
                            <div class="tulisan-3-kontenelectra-tl wrapper-flex-center justify-content-center">
                                <span style="">Location: Robotics Building ITS</span>
                                <!-- gmaps location : https://goo.gl/maps/JFS9B3uChAuiroHw9 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul">
                                <div class="tulisan-2-judul">
                                    GALLERY
                                </div>
                            </div>
                            <div class="wrapper-flex-center carousel"
                                style="justify-content: center; margin-top: 30px; width: 70%;">
                                <img src="image/IMG_9247_2.jpg" alt="" srcset="" style="width: 100%;">
                                <img src="image/IMG_9013.jpg" alt="" srcset="" style="width: 100%;">
                                <img src="image/DSC01311.jpg" alt="" srcset="" style="width: 100%;">
                                <img src="image/DSC01263.jpg" alt="" srcset="" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section8" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    REGISTER NOW!
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <a href="/home">
                                    <div class="wrapper-flex-center justify-content-center">
                                        <img id="logo-evolty" src="{{ asset('image/BARONAS/6.1 REGISTRASI.png') }}"
                                            alt="" srcset="">
                                    </div>
                                </a>
                            </div>
                            <br>
                            <br>
                            <div class="wrapper-flex-center" style="justify-content: center; align-items: center; ">
                                <img src="image/BARONAS/7. FOOTER BARONAS.png" alt="" srcset="" style="width: 70%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section9" style="margin-top: 150px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="tulisan-1" style="display: flex; justify-content: center;">
                                FIND US
                            </div>
                            <div class="wrapper-flex-center" style="justify-content: center;">
                                <img id="logo-medsos" src="image/logo-03.png" alt="" srcset=""
                                    onclick="location.href='https://www.instagram.com/evolty_its/?hl=id'">
                                <img id="logo-medsos" src="image/logo-05.png" alt="" srcset=""
                                    onclick="location.href='https://twitter.com/evolty_its'">
                                <img id="logo-medsos" src="image/logo-06.png" alt="" srcset=""
                                    onclick="location.href='https://line.me/R/ti/p/%40evolty_its'">
                            </div>
                            <div class="wrapper-flex-center"
                                style="justify-content: center; margin-top: 15px; margin-bottom: 50px;">
                                <img id="logo-berderet" src="image/logo berderet.png" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
                </!DOCTYPE>
        </section>

        </!DOCTYPE>

        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/slickcarousel.js"></script>
        <script type="text/javascript" src="js/navbar2.js"></script>
        <script type="text/javascript" src="js/popup2.js"></script>

</body>

</html>