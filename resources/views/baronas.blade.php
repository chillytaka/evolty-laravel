<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shorcut icon" href="{{asset('image/evolty.png') }}">
    <title>Selamat Datang | Baronas 2020</title>
    <link rel="stylesheet" href="{{ asset('css/style_s.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css' ) }}">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
</head>

<body style="background-image:url('/image/BARONAS/BACKGROUND BARONAS.png');">

    <div id="overlay">
        <div id="popup">
            <div id="close">X</div>
            <div class="container popke1">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Please Choose Language</h2>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 text-center">
                        <div class="kotak-bahasa" id="indo">
                            <img id="indo-flag" src="image/indonesia.png">
                            <br>
                            Indonesia
                        </div>
                    </div>
                    <div class="col-lg-6 text-center" id="english">
                        <div class="kotak-bahasa">
                            <img id="british-flag" src="image/british.png">
                            <br>
                            English
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="overlay-2">
        <div id="popup-2">
            <div id="close-2">X</div>
            <div class="container popke1">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Kami telah memperbaruhi rulebook</h2>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        
                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">                                
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Rulebook SD
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSD'">Baca</div>
                                </div>
                            </div>
                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Rulebook SMP                                   
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSMP'">Baca</div>
                                </div>
                            </div>

                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Rulebook SMA                                   
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSMA'">Baca</div>
                                </div>
                            </div>
                            
                            <div class="row justify-content-between align-items-center" style="margin-top: 15px">
                                <div class="col-md-4" style="display: flex; justify-content: space-between;">
                                    Sumo                               
                                </div>
                                <div class="col-md-4">
                                    <div class="buttonKategoriBaronas" onclick="window.location.href = '/rbSumo-ID'">Baca</div>
                                </div>
                            </div>                                                  
                            {{-- <br>
                            Rulebook SMP
                            <br>
                            Rulebook SMA
                            <br>
                            Sumo --}}
                        
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="background" style="">

        @include('partials.navbar')

        <section class="section1" style="margin-top: 100px;">
            <div class="container">
                <div class="row">
                    <div class="wrapper-flex-center d-flex justify-content-center">
                        <img id="logo-evolty" src="{{ asset('image/BARONAS/BARONAS.png') }}" alt="" srcset="">
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="section2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper-flex-center" style="justify-content: center; align-items: center;">
                        <img src="{{ asset('image/tulisan-05.png') }}" alt="" srcset="" style="width: 20%;">
                        <div class="tulisan-1"
                            style="font-size: 6vw; font-family: product_sans; font-weight: bold; letter-spacing: 7.5px;">
                            BARONAS
                        </div>
                        <img src="{{ asset('image/tulisan-05.png') }}" alt="" srcset=""
                            style="width: 20%; transform: rotate(180deg);">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="background" style="">
        <section class="section3" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="width: auto;">
                                <div class="tulisan-2-judul" style="text-align: center;">
                                    APA ITU BARONAS?
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="">
                                <span style="">Merupakan perlombaan robotika yang bertujuan untuk
                                    mengenalkan dan mengasah kemampuan para pelajar dan masyarakat umum
                                    di Indonesia dalam bidang Teknologi Robotika.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section4" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    TEMA
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="text-align:center;">
                                <span style="">"Marine Technology Development" </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div id="judul-sub-event" class="tulisan-2-judul">
                                    TOTAL HADIAH
                                </div>
                            </div>
                            <div class="tulisan-3-kontenelectrath wrapper-flex-center d-flex justify-content-center">
                                <span style="font-size:9vw;">Rp 35.000.000</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section6" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    KATEGORI
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.1 SD.png" alt="" srcset="">
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Line Tracer Analog
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (SD)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            Posisi awal robot berada di Dermaga atau Dock (start) membawa objek yang kemudian
                            dibawa melewati jalur “BARO” menuju Pelabuhan atau Port sebagai tempat untuk
                            meloading barang muatan kapal yang dibawa oleh kapten. Robot akan membawa obyek menuju
                            Dermaga kembali melalui jalur “NAS”.
                        </div>
                        <br>
                        <a href="/rbSD">
                            <button class="buttonKategoriBaronas">
                                Selengkapnya
                            </button>
                        </a>

                        <a href="/trSD">
                            <button class="buttonKategoriBaronas">
                                Download Track
                            </button>
                        </a>

                        <a href="/boxSD">
                            <button class="buttonKategoriBaronas">
                                Download Box
                            </button>
                        </a>

                    </div>
                </div>

                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-6" style="display: flex; justify-content: center;">
                        <iframe class="video_yt" src="https://www.youtube.com/embed/P-IRm8U-IWk" frameborder="0" width="350px" height="250px"></iframe>
                    </div>    
                </div>

                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.2 SMP.png" alt="" srcset="">
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Line Tracer Analog and Transporter
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (SMP)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            Tantangan yang diberikan sesuai dengan tema adalah peserta harus
                            membuat robot yang dapat melakukan beberapa misi yang meliputi
                            pengambilan penyangga, mahkota dan balok kepala. Kemudian
                            penyangga dipasang di tempatnya dan yang terakhir menempatkan
                            balok kepala dan mahkota di Singgasana Poseidon. Variasi rintangan
                            pada track juga akan menjadi ujian tersendiri bagi robot transporter.
                        </div>
                        <br>
                        <a href="/rbSMP">
                            <button class="buttonKategoriBaronas">
                                Selengkapnya
                            </button>
                        </a>
                        <a href="/trSMP">
                            <button class="buttonKategoriBaronas">
                                Download Track
                            </button>
                        </a>
                        <a href="/boxSMP">
                            <button class="buttonKategoriBaronas">
                                Download Box
                            </button>
                        </a>
                        <a href="/objSMP">
                            <button class="buttonKategoriBaronas">
                                Download Object Lomba
                            </button>
                        </a>
                    </div>
                </div>

                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-6" style="display: flex; justify-content: center;">
                        <iframe class="video_yt" src="https://www.youtube.com/embed/2EzZH4BijWc" frameborder="0" width="350px" height="250px"></iframe>
                    </div>    
                </div>
                
                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.3 SMA.png" alt="" srcset="">
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Line Tracer Mikro and Transporter
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (SMA)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            Tantangan yang diberikan sesuai dengan tema adalah
                            peserta harus mengarahkan dua robot yang harus bekerja sama
                            untuk membangun sebuah peti harta karun dari barang-barang
                            yang telah disediakan (2 balok, 1 tabung) secara berurutan dari
                            yang paling dasar yaitu balok yang diebut emas, kemudian
                            disusul balok diatas balok berlian dan yang terakhir
                            tabung disebut mahkota terletak paling atas.
                        </div>
                        <br>
                        <a href="/rbSMA">
                            <button class="buttonKategoriBaronas">
                                Selengkapnya
                            </button>
                        </a>
                        <a href="/trSMA">
                            <button class="buttonKategoriBaronas">
                                Download Track
                            </button>
                        </a>
                        <a href="/objSMA">
                            <button class="buttonKategoriBaronas">
                                Download Object Lomba
                            </button>
                        </a>
                    </div>
                </div>

                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-6" style="display: flex; justify-content: center;">
                        <iframe class="video_yt" src="https://www.youtube.com/embed/NREcC1CkwyM" frameborder="0" width="350px" height="250px"></iframe>
                    </div>    
                </div>
                
                <br>
                <br>

                <div class="row justify-content-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.4 SUMO.png" alt="" srcset="">
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Sumo
                        </div>
                        <div class="pesertaKategoriBaronas" style="">
                            (Open Category)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            Peserta membuat robot sumo yang dapat mendorong robot lawan keluar arena
                            sesuai dengan peraturan pertandingan. Operator robot mengendalikan robot
                            sumo dengan menggunakan remote wireless.
                        </div>
                        <br>
                        <a href="/rbSumo-ID">
                            <button class="buttonKategoriBaronas">
                                Selengkapnya
                            </button>
                        </a>
                    </div>
                </div>

                <br>
                <br>

                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-2 col-sm-12">
                        <img class="logoKategoriBaronas" src="image/BARONAS/4.5 UNDERWATER ROBOT.png" alt="" srcset="">
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="judulKategoriBaronas">
                            Coming Soon
                        </div>
                        {{--                         <div class="pesertaKategoriBaronas" style="">
                            (Umum + Internasional)
                        </div>
                        <div class="deskripsiKategoriBaronas" style="">
                            Underwater Robotic Competition (URC) merupakan robot bawah laut
                            yang dikendalikan jarak jauh dengan operator yang berada pada jarak
                            aman.
                        </div>
                        <br>
                        <button class="buttonKategoriBaronas" onclick="location.href='/rbUnderwater-ID'">
                            Selengkapnya
                        </button> --}}
                    </div>
                </div>

                <br>
                <br>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="wrapper-flex-center-down" style="justify-content: center; ">
                                <div class="tulisan-3-kontenelectra-tl wrapper-flex-center justify-content-center">
                                    <span style="">Lokasi Gedung Robotika ITS</span>
                                    <!-- gmaps location : https://goo.gl/maps/JFS9B3uChAuiroHw9 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <section class="section7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul">
                                <div class="tulisan-2-judul">
                                    GALERI
                                </div>
                            </div>
                            <div class="wrapper-flex-center carousel"
                                style="justify-content: center; margin-top: 30px; width: 70%;">
                                <img src="{{ asset('image/IMG_9247_2.jpg') }}" alt="" srcset="" style="width: 100%;">
                                <img src="{{ asset('image/IMG_9013.jpg') }}" alt="" srcset="" style="width: 100%;">
                                <img src="{{ asset('image/DSC01311.jpg') }}" alt="" srcset="" style="width: 100%;">
                                <img src="{{ asset('image/DSC01263.jpg') }}" alt="" srcset="" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section8" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    DAFTAR SEKARANG!
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <a href="/home">
                                    <div class="wrapper-flex-center justify-content-center">
                                        <img id="logo-evolty" src="{{ asset('image/BARONAS/6.1 REGISTRASI.png') }}"
                                            alt="" srcset="">
                                    </div>
                                </a>
                            </div>
                            <br>
                            <br>
                            <div class="wrapper-flex-center" style="justify-content: center; align-items: center; ">
                                <img src="image/BARONAS/7. FOOTER BARONAS.png" alt="" srcset="" style="width: 70%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section9" style="margin-top: 150px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="tulisan-1" style="display: flex; justify-content: center;">
                                FIND US
                            </div>
                            <div class="wrapper-flex-center" style="justify-content: center;">
                                <img id="logo-medsos" src="image/logo-03.png" alt="" srcset=""
                                    onclick="location.href='https://www.instagram.com/evolty_its/?hl=id'">
                                <img id="logo-medsos" src="image/logo-05.png" alt="" srcset=""
                                    onclick="location.href='https://twitter.com/evolty_its'">
                                <img id="logo-medsos" src="image/logo-06.png" alt="" srcset=""
                                    onclick="location.href='https://line.me/R/ti/p/%40evolty_its'">
                            </div>
                            <div class="wrapper-flex-center"
                                style="justify-content: center; margin-top: 15px; margin-bottom: 50px;">
                                <img id="logo-berderet" src="image/logo berderet.png" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript" src="js/slickcarousel.js"></script>
    <script src="js/popup.js"></script>
    <script type="text/javascript" src="js/navbar2.js"></script>

</body>

</html>