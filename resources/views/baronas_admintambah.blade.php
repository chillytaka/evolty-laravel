@extends('layouts.dashboard')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Registrasi Baronas</h1>
</div>

<div class="container">

    <form method="post" action="/admin/baronas/store" enctype="multipart/form-data">

        {{ csrf_field() }}
       

        <div class="form-group">
            <label>NamaTim</label>
            <textarea name="namatim" class="form-control"
                placeholder=" Nama tim .."></textarea>

            @if($errors->has('namatim'))
            <div class="text-danger">
                {{ $errors->first('namatim')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Email Ketua</label>
            <input type="text" name="emailketua" class="form-control" placeholder="Email ketua ..">

            @if($errors->has('emailketua'))
            <div class="text-danger">
                {{ $errors->first('emailketua')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Kategori Lomba</label>
            <select name="kategorilomba" class="form-control">
                <option value="SD" >SD</option>
                <option value="SMP" >SMP</option>
                <option value="SMA" >SMA</option>
                <option value="Umum - Sumo" >Umum - Sumo</option>
                <option value="Umum - ROV" >Umum- ROV</option> 
            </select>

            @if($errors->has('kategorilomba'))
            <div class="text-danger">
                {{ $errors->first('kategorilomba')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Asal Instansi</label>
            <textarea name="asalinstansi" class="form-control"
                placeholder="Asal Instansi  .."> </textarea>

            @if($errors->has('asalinstansi'))
            <div class="text-danger">
                {{ $errors->first('asalinstansi')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Alamat Instansi</label>
            <textarea name="alamatinstansi" class="form-control"
                placeholder="Alamat Instansi  ..">  </textarea>

            @if($errors->has('alamatinstansi'))
            <div class="text-danger">
                {{ $errors->first('alamatinstansi')}}
            </div>
            @endif

        </div>

        <div class="form-group">

            <label>Nama Pembimbing</label>
            <textarea name="nama_pembimbing" class="form-control"
                placeholder="Nama Pembimbing ..">  </textarea>

            @if($errors->has('nama_pembimbing'))
            <div class="text-danger">
                {{ $errors->first('nama_pembimbing')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 1</label>
            <textarea name="nama_anggota1" class="form-control"
                placeholder="Nama Anggota 1 ..">  </textarea>

            @if($errors->has('nama_anggota1'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota1')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 2</label>
            <textarea name="nama_anggota2" class="form-control"
                placeholder="Nama Anggota 2 ..">  </textarea>

            @if($errors->has('nama_anggota2'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota2')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 3</label>
            <textarea name="nama_anggota3" class="form-control"
                placeholder="Nama Anggota 3 .."> </textarea>

            @if($errors->has('nama_anggota3'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota3')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 4</label>
            <textarea name="nama_anggota4" id="anggota4" class="form-control"
                placeholder="Nama Anggota 4 ..">  </textarea>

            @if($errors->has('nama_anggota4'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota4')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nomor Handphone</label>
            <textarea name="phonenumber" class="form-control"
                placeholder="Nomor Handphone ..">  </textarea>

            @if($errors->has('phonenumber'))
            <div class="text-danger">
                {{ $errors->first('phonenumber')}}
            </div>
            @endif
        </div>

        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">

                <div class="custom-file">

                    <label>File Kartu Pelajar / KTP Anggota 1</label>
                    <input type="file" class="form-control-file" name="filektmanggota1">

                </div>

                @if($errors->has('filektmanggota1'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota1')}}
                </div>
                @endif

            </div>

            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 1</label>
                    <input type="file" class="form-control-file" name="filefotoanggota1">
                </div>

                @if($errors->has('filefotoanggota1'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota1')}}
                </div>
                @endif

            </div>
        </div>


        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Kartu Pelajar / KTP Anggota 2</label>

                    <input type="file" class="form-control-file" name="filektmanggota2">
                </div>

                @if($errors->has('filektmanggota2'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota2')}}
                </div>
                @endif

            </div>


            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 2</label>

                    <input type="file" class="form-control-file" name="filefotoanggota2">
                </div>

                @if($errors->has('filefotoanggota2'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota2')}}
                </div>
                @endif

            </div>
        </div>

        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">
                <div class="custom-file">

                    <label>File Kartu Pelajar / KTP Anggota 3</label>


                    <input type="file" class="form-control-file" name="filektmanggota3">
                </div>

                @if($errors->has('filektmanggota3'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota3')}}
                </div>
                @endif

            </div>

            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 3</label>


                    <input type="file" class="form-control-file" name="filefotoanggota3">
                </div>

                @if($errors->has('filefotoanggota3'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota3')}}
                </div>
                @endif

            </div>
        </div>

        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">
                <div class="custom-file">

                    <label>File Kartu Pelajar / KTP Anggota 4</label>


                    <input type="file" id="anggota4" class="form-control-file" name="filektmanggota4">
                </div>

                @if($errors->has('filektmanggota4'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota4')}}
                </div>
                @endif

            </div>

            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 4</label>

                    <input type="file" id="anggota4" class="form-control-file" name="filefotoanggota4">
                </div>

                @if($errors->has('filefotoanggota4'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota4')}}
                </div>
                @endif

            </div>
        </div>

        <div class="form-group">
            <label>
                <b>File Bukti Pembayaran</b>
            </label>
            <input type="file" name="filebuktipembayaran">

        </div>

        @if($errors->has('filebuktipembayaran'))
        <div class="text-danger">
            {{ $errors->first('filebuktipembayaran') }}
        </div>
        @endif
</div>


<div class="form-group">
    <input type="submit" class="btn btn-success value="Simpan">
</div>

</form>

</div>
@endsection