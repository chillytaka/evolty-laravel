@extends('layouts.dashboard')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Baronas</h1>
</div>

@if ($baronas->nopendaftaran)
<div class="alert alert-success" role="alert">
    Data anda sudah diverifikasi. No Peserta: {{$baronas->nopendaftaran}}
</div>
<div class="alert alert-success" role="alert">
    Simpan bukti pembayaran anda untuk digunakan saat lomba nanti.
    <a href="{{url('/baronas_file/kwitansi/'. $baronas->kwitansi)}}">download kwitansi</a>
</div>
@endif

<div class="container">

    <form method="post" action="/home/baronas/update/{{ $baronas->id }}" enctype="multipart/form-data">

        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="form-group">
            <label>NamaTim</label>
            <textarea name="namatim" class="form-control"
                placeholder=" Nama tim .."> {{ $baronas->namatim }} </textarea>

            @if($errors->has('namatim'))
            <div class="text-danger">
                {{ $errors->first('namatim')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Kategori Lomba</label>
            <select name="kategorilomba" class="form-control">
                <option disabled value="SD" {{$baronas->kategori === 'SD' ? 'selected' : 'disabled'}}>SD</option>
                <option value="SMP" {{ $baronas->kategori === "SMP" ? "selected" : null }}>SMP</option>
                <option value="SMA" {{ $baronas->kategori === "SMA" ? "selected" : "disabled" }}>SMA</option>
                <option value="Umum - Sumo" {{$baronas->kategori === 'Umum - Sumo' ? 'selected' : null}}>Umum - Sumo
                </option>
                {{--                 <option value="Umum - ROV" {{$baronas->kategori === 'Umum - ROV' ? 'selected' : null}}>Umum
                - ROV
                </option> --}}
            </select>

            @if($errors->has('kategorilomba'))
            <div class="text-danger">
                {{ $errors->first('kategorilomba')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Asal Instansi</label>
            <textarea name="asalinstansi" class="form-control"
                placeholder="Asal Instansi  .."> {{ $baronas->asalinstansi }} </textarea>

            @if($errors->has('asalinstansi'))
            <div class="text-danger">
                {{ $errors->first('asalinstansi')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Alamat Instansi</label>
            <textarea name="alamatinstansi" class="form-control"
                placeholder="Alamat Instansi  .."> {{ $baronas->alamatinstansi }} </textarea>

            @if($errors->has('alamatinstansi'))
            <div class="text-danger">
                {{ $errors->first('alamatinstansi')}}
            </div>
            @endif

        </div>

        <div class="form-group">

            <label>Nama Pembimbing</label>
            <textarea name="nama_pembimbing" class="form-control"
                placeholder="Nama Pembimbing .."> {{ $baronas->namapembimbing }} </textarea>

            @if($errors->has('nama_pembimbing'))
            <div class="text-danger">
                {{ $errors->first('nama_pembimbing')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 1</label>
            <textarea name="nama_anggota1" class="form-control"
                placeholder="Nama Anggota 1 .."> {{ $baronas->namaanggota1 }} </textarea>

            @if($errors->has('nama_anggota1'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota1')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 2</label>
            <textarea name="nama_anggota2" class="form-control"
                placeholder="Nama Anggota 2 .."> {{ $baronas->namaanggota2 }} </textarea>

            @if($errors->has('nama_anggota2'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota2')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 3</label>
            <textarea name="nama_anggota3" class="form-control"
                placeholder="Nama Anggota 3 .."> {{ $baronas->namaanggota3 }} </textarea>

            @if($errors->has('nama_anggota3'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota3')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nama Anggota 4</label>
            <textarea name="nama_anggota4" id="anggota4" disabled class="form-control"
                placeholder="Nama Anggota 4 .."> {{ $baronas->namaanggota4 }} </textarea>

            @if($errors->has('nama_anggota4'))
            <div class="text-danger">
                {{ $errors->first('nama_anggota4')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nomor Handphone</label>
            <textarea name="phonenumber" class="form-control"
                placeholder="Nomor Handphone .."> {{ $baronas->phone_number }} </textarea>

            @if($errors->has('phonenumber'))
            <div class="text-danger">
                {{ $errors->first('phonenumber')}}
            </div>
            @endif
        </div>

        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">

                <div class="custom-file">

                    <label>File Kartu Pelajar / KTP Anggota 1</label>

                    @if($baronas->ktmanggota1)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/ktm_anggota1/'.$baronas->ktmanggota1) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" class="form-control-file" name="filektmanggota1">

                </div>

                @if($errors->has('filektmanggota1'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota1')}}
                </div>
                @endif

            </div>

            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 1</label>

                    @if($baronas->fotoanggota1)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/foto_anggota1/'.$baronas->fotoanggota1) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" class="form-control-file" name="filefotoanggota1">
                </div>

                @if($errors->has('filefotoanggota1'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota1')}}
                </div>
                @endif

            </div>
        </div>


        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Kartu Pelajar / KTP Anggota 2</label>

                    @if($baronas->ktmanggota2)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/ktm_anggota2/'.$baronas->ktmanggota2) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" class="form-control-file" name="filektmanggota2">
                </div>

                @if($errors->has('filektmanggota2'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota2')}}
                </div>
                @endif

            </div>


            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 2</label>

                    @if($baronas->fotoanggota2)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/foto_anggota2/'.$baronas->fotoanggota2) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" class="form-control-file" name="filefotoanggota2">
                </div>

                @if($errors->has('filefotoanggota2'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota2')}}
                </div>
                @endif

            </div>
        </div>

        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">
                <div class="custom-file">

                    <label>File Kartu Pelajar / KTP Anggota 3</label>

                    @if($baronas->ktmanggota3)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/ktm_anggota3/'.$baronas->ktmanggota3) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" class="form-control-file" name="filektmanggota3">
                </div>

                @if($errors->has('filektmanggota3'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota3')}}
                </div>
                @endif

            </div>

            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 3</label>

                    @if($baronas->fotoanggota3)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/foto_anggota3/'.$baronas->fotoanggota3) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" class="form-control-file" name="filefotoanggota3">
                </div>

                @if($errors->has('filefotoanggota3'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota3')}}
                </div>
                @endif

            </div>
        </div>

        <div class="form-row justify-items-center">
            <div class="form-group col-md-6">
                <div class="custom-file">

                    <label>File Kartu Pelajar / KTP Anggota 4</label>

                    @if($baronas->ktmanggota4)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/ktm_anggota4/'.$baronas->ktmanggota4) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" disabled id="anggota4" class="form-control-file" name="filektmanggota4">
                </div>

                @if($errors->has('filektmanggota4'))
                <div class="text-danger">
                    {{ $errors->first('filektmanggota4')}}
                </div>
                @endif

            </div>

            <div class="form-group col-md-6">
                <div class="custom-file">
                    <label>File Foto Anggota 4</label>

                    @if($baronas->fotoanggota4)
                    <div class="text-success">File Sudah Diupload
                        <a href="{{ url('/baronas_file/foto_anggota4/'.$baronas->fotoanggota4) }}">
                            lihat file</a>
                    </div>
                    @endif

                    <input type="file" disabled id="anggota4" class="form-control-file" name="filefotoanggota4">
                </div>

                @if($errors->has('filefotoanggota4'))
                <div class="text-danger">
                    {{ $errors->first('filefotoanggota4')}}
                </div>
                @endif

            </div>
        </div>

        <div class="form-group">
            <label>
                <b>File Bukti Pembayaran</b>
            </label>

            @if($baronas->buktitransfer)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/baronas_file/buktibayar/'.$baronas->buktitransfer) }}">
                    lihat file</a>
            </div>
            @endif
            <input type="file" name="filebuktipembayaran">

        </div>

        @if($errors->has('filebuktipembayaran'))
        <div class="text-danger">
            {{ $errors->first('filebuktipembayaran') }}
        </div>
        @endif
</div>


<div class="form-group">
    <input type="submit" class="btn btn-success @if ($baronas->nopendaftaran)
               disabled 
            @endif" @if ($baronas->nopendaftaran)
    disabled
    @endif value="Simpan">
</div>

</form>

</div>
@endsection
