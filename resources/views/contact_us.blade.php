<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shorcut icon" href="image/evolty.png">
    <title>contact | Evolty 2020</title>
    <link rel="stylesheet" href="css/style_s.css">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">

</head>

<body style="background-image:url('image/CONTACT US/BG.png');">

    <div class="background" style="">

        @include('partials.navbar')

    </div>

    <div class="background">
        <section class="section2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center" style="justify-content: center; align-items: center;">
                            <img src="image/tulisan-05.png" alt="" srcset="" style="width: 20%;">
                            <div class="tulisan-1"
                                style="font-size: 5vw; font-family: product_sans; font-weight: bold; letter-spacing: 7.5px; text-align: center;">
                                CONTACT US
                            </div>
                            <img src="image/tulisan-05.png" alt="" srcset=""
                                style="width: 20%; transform: rotate(180deg);">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div id="judul-sub-event" class="tulisan-2-judul-contact">
                                    EVOLTY
                                </div>
                            </div>
                            <div class="tulisan-3-contact wrapper-flex-center d-flex justify-content-center">
                                <span class="no-italics">MUHAMMAD DAFFA CAHYONOPUTRA <br> (KETUA) <br> 0818739017</span>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div id="judul-sub-event" class="tulisan-2-judul-contact">
                                    ELECTRA
                                </div>
                            </div>
                            <div class="tulisan-3-contact wrapper-flex-center d-flex justify-content-center">
                                <span class="no-italics">BAGAS <br> 089630772860 <br> electra9its@gmail.com</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div id="judul-sub-event" class="tulisan-2-judul-contact">
                                    BARONAS
                                </div>
                            </div>
                            <div class="tulisan-3-contact wrapper-flex-center d-flex justify-content-center">
                                <span class="no-italics">HAIDAR (KOORDINATOR) <br> 081217949107</span>
                            </div>
                            <br>
                            <br>
                            <div class="tulisan-3-contact wrapper-flex-center d-flex justify-content-center">
                                <span class="no-italics">NAFIS (RULES) <br> 085335233203</span>
                            </div>
                            <br>
                            <br>
                            <div class="tulisan-3-contact wrapper-flex-center d-flex justify-content-center">
                                <span class="no-italics">GALIH (REGISTRASI) <br> 085239564858</span>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section6" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="wrapper-flex-center" style="justify-content: center; align-items: center; ">
                                <img src="image/ELECTRA/10. FOOTER ELECTRA.png" alt="" srcset="" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section7" style="margin-top: 150px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="tulisan-1" style="display: flex; justify-content: center;">
                                FIND US
                            </div>
                            <div class="wrapper-flex-center" style="justify-content: center;">
                                <img id="logo-medsos" src="image/logo-03.png" alt="" srcset=""
                                    onclick="location.href='https://www.instagram.com/evolty_its/?hl=id'">
                                <img id="logo-medsos" src="image/logo-05.png" alt="" srcset=""
                                    onclick="location.href='https://twitter.com/evolty_its'">
                                <img id="logo-medsos" src="image/logo-06.png" alt="" srcset=""
                                    onclick="location.href='https://line.me/R/ti/p/%40evolty_its'">
                            </div>
                            <div class="wrapper-flex-center"
                                style="justify-content: center; margin-top: 15px; margin-bottom: 50px;">
                                <img id="logo-berderet" src="image/logo berderet.png" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript" src="js/slickcarousel.js"></script>
    <script type="text/javascript" src="js/navbar2.js"></script>

</body>

</html>