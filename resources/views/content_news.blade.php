<html>

<head>
    <meta charset="utf-8">
    <title>News | EVOLTY</title>
    <link rel="shorcut icon" href="{{asset('image/evolty.png') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script src='https://ricostacruz.com/jquery.transit/jquery.transit.min.js'></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="/css/style_s.css">
    <link rel="stylesheet" href="/css/style_s.css">
    <link rel="stylesheet" type="text/css" href="/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/css/slick-theme.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <link rel="stylesheet" href="/css/news_content.css">
</head>

<body>

    @include('partials.navbar')

    <div id="particles"></div>
    <div class="container">
        <div class="row align-content-center justify-content-center ">
            <div class="col-lg-6 col-md-6 animated fadeInLeft" id="logo-login">
                @if ($news->path_headline)
                <img src="/news_file/headline/{{$news->path_headline}}" id="logomage" style="width:50em">
                @endif
            </div>
            <div class="col-lg-6 col-md-6 animated fadeInRight card" id="isi-login"
                style="max-height: 35em;overflow-y: auto;">
                <div class="card-body">
                    <h1 class="card-title" style="color:black">{{$news->title}}</h1>
                    <p class="card-subtitle text-muted">{{$news->updated_at}}</p>
                    <hr>

                    @if ($news->path_headline)
                    <img src="/news_file/headline/{{$news->path_headline}}" style="width:100%">
                    @endif
                    <div class="main_content">
                        {!!$content!!}
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script src="{{asset('js/restrict.js') }}"></script>
    <script type="text/javascript" src="/js/navbar2.js"></script>

</body>