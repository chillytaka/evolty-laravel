@extends('layouts.dashboard')

@section('content')


<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  <h4 class="h5 mb-0 text-gray-800">Click to go to dashboard</h4>
</div>

@if($errors->has('failed'))
<div class="alert alert-danger" role="alert">
  {{ $errors->first('failed')}}
</div>
@endif

<!-- Content Row -->
<div class="row">
  <!-- Mage -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="font-weight-bold text-warning text-uppercase mb-1">
              MAGE
            </div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">
              Sudah Ditutup
            </div>
            <a href="#" class="btn btn-warning disabled btn-icon-split btn-block mt-4 mb-0">
              <span class="text">lihat dashboard</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Electra -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2" onclick="location.href='/home/electra/edit'">
      {{-- <div class="card border-left-info shadow h-100 py-2"> --}}
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col">
            <div class="font-weight-bold text-info text-uppercase mb-1">
              Electra
            </div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">
              Sudah Ditutup
            </div>
            <a href="/home/electra/edit" class="btn btn-info btn-icon-split btn-block mt-4 mb-0">
              <span class="text">lihat dashboard</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Baronas -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2" onclick="location.href='#'">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col">
            <div class="font-weight-bold text-success text-uppercase mb-1">
              Baronas
            </div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">
              Pendaftaran Dibuka
            </div>
            <a href="/home/baronas/edit" class="btn btn-success btn-icon-split btn-block mt-4 mb-0">
              <span class="text">lihat dashboard</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Evolve -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="font-weight-bold text-primary text-uppercase mb-1">
              Evolve
            </div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">
              Coming Soon
            </div>

            <a href="#" class="btn btn-primary disabled btn-icon-split btn-block mt-4 mb-0">
              <span class="text">lihat dashboard</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>


  @endsection