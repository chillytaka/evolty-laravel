<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shorcut icon" href="{{asset('image/evolty.png') }}">
    <title>Selamat Datang | Electra 2020</title>
    <link rel="stylesheet" href="{{ asset('css/style_s.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css' ) }}">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
</head>

<body style="background-image:url('image/ELECTRA/BACKGROUND ELECTRA.png');">
    <div class="background" style="">

        @include('partials.navbar')

        <section class="section1" style="margin-top: 100px;">
            <div class="container">
                <div class="row">
                    <div class="wrapper-flex-center d-flex justify-content-center">
                        <img id="logo-evolty" src="{{ asset('image/ELECTRA/ELECTRA.png') }}" alt="" srcset="">
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="section2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper-flex-center" style="justify-content: center; align-items: center;">
                        <img src="{{ asset('image/tulisan-05.png') }}" alt="" srcset="" style="width: 20%;">
                        <div class="tulisan-1"
                            style="font-size: 6vw; font-family: product_sans; font-weight: bold; letter-spacing: 7.5px;">
                            ELECTRA
                        </div>
                        <img src="{{ asset('image/tulisan-05.png') }}" alt="" srcset=""
                            style="width: 20%; transform: rotate(180deg);">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="background" style="">
        <section class="section3" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="width: auto;">
                                <div class="tulisan-2-judul">
                                    APA ITU ELECTRA?
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="">
                                <span style="">Sebuah Olimpiade dalam bidang elektro yang
                                    meliputi mata pelajaran matematika, fisika, rangkaian listrik, logika dan
                                    pemrograman dengan
                                    tujuan mengenalkan Fakultas Teknologi Elektro kepada siswa SMA yang ingin
                                    melanjutkan studi
                                    di perguruan tinggi dan membuka kesempatan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section4" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    TEMA
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="text-align:center;">
                                <span style="">"Involving Young Generation in Technology Advancement" </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div id="judul-sub-event" class="tulisan-2-judul">
                                    TOTAL HADIAH
                                </div>
                            </div>
                            <div class="tulisan-3-kontenelectrath wrapper-flex-center d-flex justify-content-center">
                                <span style="font-size:9vw;">Rp 17.000.000</span>

                            </div>
                            <div class="tulisan-3-kontenelectrath wrapper-flex-center d-flex justify-content-center">
                                <span style="">+FREEPASS TEKNIK ELEKTRO ITS</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section6" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    MATA PELAJARAN
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="text-align:center;">
                                <span style="">Fisika, Matematika, Rangkaian Listrik Dasar, dan Algoritma pemrograman
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section8" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    TIMELINE
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="wrapper-flex-center d-flex justify-content-center justify-content-lg-end">
                            <img id="logo-evolty" src="{{ asset('image/ELECTRA/5.1 PENYISIHAN.png') }}" alt=""
                                srcset="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6" style="text-align:center;">
                        <div class="wrapper-flex-centertl" style="align-items: center; height: 100%;">
                            <div class="wrapper-flex-center-down align-items-lg-start">
                                <div class="tulisan-3-kontenelectra-tl">
                                    Penyisihan
                                </div>
                                <div class="tulisan-3-kontenelectra-tldate " style="">
                                    2 Feb 2019
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="wrapper-flex-center d-flex justify-content-center justify-content-lg-end">
                            <img id="logo-evolty" src="{{ asset('image/ELECTRA/5.2 SEMI FINAL.png') }}" alt=""
                                srcset="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6" style="text-align:center;">
                        <div class="wrapper-flex-centertl" style="align-items: center; height: 100%;">
                            <div class="wrapper-flex-center-down align-items-lg-start">
                                <div class="tulisan-3-kontenelectra-tl" style="">
                                    Semifinal
                                </div>
                                <div class="tulisan-3-kontenelectra-tldate" style="">
                                    8 Feb 2019
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="wrapper-flex-center d-flex justify-content-center justify-content-lg-end">
                            <img id="logo-evolty" src="{{ asset('image/ELECTRA/5.3 FINAL.png') }}" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6" style="text-align:center;">
                        <div class="wrapper-flex-centertl" style="align-items: center; height: 100%;">
                            <div class="wrapper-flex-center-down align-items-lg-start">
                                <div class="tulisan-3-kontenelectra-tl" style="">
                                    Final
                                </div>
                                <div class="tulisan-3-kontenelectra-tldate" style="">
                                    9 Feb 2019
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center; ">
                            <div class="tulisan-3-kontenelectra-tl wrapper-flex-center justify-content-center">
                                <span style="">LOKASI FTE ITS</span>
                                <!-- gmaps location : https://goo.gl/maps/JFS9B3uChAuiroHw9 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section6" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    PENYISIHAN
                                </div>
                            </div>
                            <div class="tulisan-3-konten wrapper-flex-center justify-content-center">
                                <span style="">Offline & Online</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section6" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul"> REGION
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="">
                                <span style="">Bali Balikpapan Banyuwangi Gresik Jabodetabek Jember Kediri
                                    Madiun Madura Malang Mojokerto Probolinggo Semarang Solo Surabaya Tuban</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section8">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul">
                                <div class="tulisan-2-judul">
                                    GALERI
                                </div>
                            </div>
                            <div class="wrapper-flex-center carousel"
                                style="justify-content: center; margin-top: 30px; width: 70%;">
                                <img src="{{ asset('image/1 Penyisihan (81).jpg') }}" alt="" srcset=""
                                    style="width: 100%;">
                                <img src="{{ asset('image/4 FINAL (75).jpg') }}" alt="" srcset="" style="width: 100%;">
                                <img src="{{ asset('image/1 Penyisihan (40).jpg') }}" alt="" srcset=""
                                    style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section6" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    DAFTAR SEKARANG!
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <a href="/home">
                                    <div class="wrapper-flex-center justify-content-center">
                                        <img id="logo-evolty" src="{{ asset('image/ELECTRA/9.1 REGISTRASI.png') }}"
                                            alt="" srcset="">
                                    </div>
                                </a>
                            </div>
                            <br>
                            <br>
                            <div class="wrapper-flex-center" style="justify-content: center; align-items: center; ">
                                <img src="{{ asset('image/ELECTRA/10. FOOTER ELECTRA.png') }}" alt="" srcset=""
                                    style="width: 70%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section7" style="margin-top: 150px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="tulisan-1" style="display: flex; justify-content: center;">
                                FIND US
                            </div>
                            <div class="wrapper-flex-center" style="justify-content: center;">
                                <img id="logo-medsos" src="{{ asset('image/logo-03.png') }}" alt="" srcset=""
                                    onclick="location.href='https://www.instagram.com/evolty_its/?hl=id'">
                                <img id="logo-medsos" src="{{ asset('image/logo-05.png') }}" alt="" srcset=""
                                    onclick="location.href='https://twitter.com/evolty_its'">
                                <img id="logo-medsos" src="{{ asset('image/logo-06.png') }}" alt="" srcset=""
                                    onclick="location.href='https://line.me/R/ti/p/%40evolty_its'">
                            </div>
                            <div class="wrapper-flex-center"
                                style="justify-content: center; margin-top: 15px; margin-bottom: 50px;">
                                <img id="logo-berderet" src="{{ asset('image/logo berderet.png') }}" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slickcarousel.js') }}"></script>
    <script type="text/javascript" src="js/navbar2.js"></script>

</body>

</html>