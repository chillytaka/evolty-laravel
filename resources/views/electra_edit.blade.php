@extends('layouts.dashboard')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Electra</h1>
</div>

@if ($electra[0]->nopendaftaran)
<div class="alert alert-success" role="alert">
    Data anda sudah diverifikasi. No Peserta: {{$electra[0]->nopendaftaran}}
</div>
<div class="alert alert-success" role="alert">
    Simpan bukti pembayaran anda untuk digunakan saat lomba nanti.
    <a href="{{url('/electra_file/kwitansi/'. $electra[0]->kwitansi)}}">download kwitansi</a>
</div>
@endif

<div class="container">

    <form method="post" action="/home/electra/update/{{ $electra[0]->id }}" enctype="multipart/form-data">

        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="form-group">
            <label>NamaTim</label>
            <textarea name="namatim" class="form-control"
                placeholder=" Nama tim .."> {{ $electra[0]->namatim }} </textarea>

            @if($errors->has('namatim'))
            <div class="text-danger">
                {{ $errors->first('namatim')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Nama Ketua</label>
            <textarea name="namaketua" class="form-control"
                placeholder="Nama ketua .."> {{ $electra[0]->namaketua }} </textarea>

            @if($errors->has('namaketua'))
            <div class="text-danger">
                {{ $errors->first('namaketua')}}
            </div>
            @endif

        </div>

        <div class="form-group">

            <label>Nama Anggota</label>
            <textarea name="namaanggota" class="form-control"
                placeholder="Nama anggota .."> {{ $electra[0]->namaanggota }} </textarea>

            @if($errors->has('namaanggota'))
            <div class="text-danger">
                {{ $errors->first('namaanggota')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Asal Sekolah</label>
            <textarea name="asalsekolah" class="form-control"
                placeholder="Asal sekolah .."> {{ $electra[0]->asalsekolah }} </textarea>

            @if($errors->has('asalsekolah'))
            <div class="text-danger">
                {{ $errors->first('asalsekolah')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nomor Handphone</label>
            <textarea name="phonenumber" class="form-control"
                placeholder="Nomor Handphone .."> {{ $electra[0]->phone_number }} </textarea>

            @if($errors->has('phonenumber'))
            <div class="text-danger">
                {{ $errors->first('phonenumber')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>tipe pendaftaran</label>
            <select name="tipependaftaran" class="form-control">
                <option value="Online" {{$electra[0]->tipependaftaran === 'Online' ? 'selected' : null}}>Online</option>

                <option value="Offline" {{$electra[0]->tipependaftaran == 'Offline' ? 'selected' : null}}>
                    Offline</option>
            </select>

            @if($errors->has('tipependaftaran'))
            <div class="text-danger">
                {{ $errors->first('tipependaftaran')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>region</label>
            <select name="region" class="form-control">
                <option value="Online" {{$electra[0]->region === 'Online' ? 'selected' : null}}>Online</option>
                <option value="Banyuwangi" {{$electra[0]->region === 'Banyuwangi' ? 'selected' : null}}>
                    Banyuwangi</option>
                <option value="Probolinggo" {{$electra[0]->region === 'Probolinggo' ? 'selected' : null}}>
                    Probolinggo</option>
                <option value="Malang" {{$electra[0]->region === 'Malang' ? 'selected' : null}}>Malang</option>
                <option value="Jember" {{$electra[0]->region === 'Jember' ? 'selected' : null}}>Jember</option>
                <option value="Solo" {{$electra[0]->region === 'Solo' ? 'selected' : null}}>Solo</option>
                <option value="Madura" {{$electra[0]->region === 'Madura' ? 'selected' : null}}>Madura</option>
                <option value="Gresik" {{$electra[0]->region === 'Gresik' ? 'selected' : null}}>Gresik</option>
                <option value="Bali" {{$electra[0]->region === 'Bali' ? 'selected' : null}}>Bali</option>
                <option value="Surabaya" {{$electra[0]->region === 'Surabaya' ? 'selected' : null}}>Surabaya
                </option>
                <option value="Sidoarjo" {{$electra[0]->region === 'Sidoarjo' ? 'selected' : null}}>Sidoarjo
                </option>
                <option value="Madiun" {{$electra[0]->region === 'Madiun' ? 'selected' : null}}>Madiun</option>
                <option value="Mojokerto" {{$electra[0]->region === 'Mojokerto' ? 'selected' : null}}>Mojokerto
                </option>
                <option value="Jabodetabek" {{$electra[0]->region === 'Jabodetabek' ? 'selected' : null}}>
                    Jabodetabek</option>
                <option value="Kalimantan" {{$electra[0]->region === 'Kalimantan' ? 'selected' : null}}>
                    Kalimantan</option>
                <option value="Kediri" {{$electra[0]->region === 'Kediri' ? 'selected' : null}}>Kediri</option>
                <option value="Semarang" {{$electra[0]->region === 'Semarang' ? 'selected' : null}}>Semarang
                </option>
                <option value="Tuban" {{$electra[0]->region === 'Tuban' ? 'selected' : null}}>Tuban</option>
                <option value="Lumajang" {{$electra[0]->region === 'Lumajang' ? 'selected' : null}}>Lumajang
                </option>
            </select>

            @if($errors->has('region'))
            <div class="text-danger">
                {{ $errors->first('region')}}
            </div>
            @endif

        </div>


        <div class="form-group">
            <label>
                <b>File Bukti Pembayaran</b>
            </label>

            @if($electra[0]->buktitransfer)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/electra_file/buktibayar/'.$electra[0]->buktitransfer) }}">
                    lihat file</a>
            </div>
            @endif
            <input type="file" name="filebuktipembayaran">

            <div>Anda akan diberikan kesempatan 3 hari untuk melakukan upload bukti pembayaran
            </div>

            @if($errors->has('filebuktipembayaran'))
            <div class="text-danger">
                {{ $errors->first('filebuktipembayaran') }}
            </div>
            @endif
        </div>


        <div class="form-group">
            <input type="submit" class="btn btn-success @if ($electra[0]->nopendaftaran)
               disabled 
            @endif" @if ($electra[0]->nopendaftaran)
            disabled
            @endif value="Simpan">
        </div>

    </form>

</div>
@endsection