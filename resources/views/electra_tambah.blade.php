@extends('layouts.dashboard')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Registrasi Electra</h1>
</div>

<div class="container">

    <form method="post" action="/home/electra/store" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="form-group">
            <label>NamaTim</label>
            <input type="text" name="namatim" class="form-control" placeholder="Nama tim ..">

            @if($errors->has('namatim'))
            <div class="text-danger">
                {{ $errors->first('namatim')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Nama Ketua</label>
            <input type="text" name="namaketua" class="form-control" placeholder="Nama ketua ..">

            @if($errors->has('namaketua'))
            <div class="text-danger">
                {{ $errors->first('namaketua')}}
            </div>
            @endif

        </div>

        <div class="form-group">

            <label>Nama Anggota</label>
            <input type="text" name="namaanggota" class="form-control" placeholder="Nama anggota ..">

            @if($errors->has('namaanggota'))
            <div class="text-danger">
                {{ $errors->first('namaanggota')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Asal Sekolah</label>
            <input type="text" name="asalsekolah" class="form-control" placeholder="Asal sekolah ..">

            @if($errors->has('asalsekolah'))
            <div class="text-danger">
                {{ $errors->first('asalsekolah')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Nomor Handphone</label>
            <input type="tel" name="phonenumber" class="form-control" placeholder="Nomor Handphone ..">

            @if($errors->has('phonenumber'))
            <div class="text-danger">
                {{ $errors->first('phonenumber')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Tipe Pendaftaran</label>
            <select name="tipependaftaran" class="form-control">
                <option value="Online">Online</option>
                <option value="Offline">Offline</option>
            </select>

            @if($errors->has('tipependaftaran'))
            <div class="text-danger">
                {{ $errors->first('tipependaftaran')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Region</label>
            <select name="region" class="form-control">
                <option value="Online">Online</option>
                <option value="Banyuwangi">Banyuwangi</option>
                <option value="Probolinggo">Probolinggo</option>
                <option value="Malang">Malang</option>
                <option value="Jember">Jember</option>
                <option value="Solo">Solo</option>
                <option value="Madura">Madura</option>
                <option value="Gresik">Gresik</option>
                <option value="Bali">Bali</option>
                <option value="Surabaya">Surabaya</option>
                <option value="Sidoarjo">Sidoarjo</option>
                <option value="Madiun">Madiun</option>
                <option value="Mojokerto">Mojokerto</option>
                <option value="Jabodetabek">Jabodetabek</option>
                <option value="Kalimantan">Kalimantan</option>
                <option value="Kediri">Kediri</option>
                <option value="Semarang">Semarang</option>
                <option value="Tuban">Tuban</option>
                <option value="Lumajang">Lumajang</option>
            </select>


            @if($errors->has('region'))
            <div class="text-danger">
                {{ $errors->first('region')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <div class="custom-file">
                <label>File Bukti Pembayaran</label>
                <input type="file" class="form-control-file" name="filebuktipembayaran">
                <div>Anda akan diberikan kesempatan 3 hari untuk melakukan upload bukti pembayaran
                </div>
            </div>

            @if($errors->has('filebuktipembayaran'))
            <div class="text-danger">
                {{ $errors->first('filebuktipembayaran')}}
            </div>
            @endif
        </div>


        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>

    </form>

</div>
@endsection