<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shorcut icon" href="image/evolty.png">
    <title>Selamat Datang | Evolve 2020</title>
    <link rel="stylesheet" href="css/style_s.css">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
</head>

<body style="background-image:url('image/EVOLVE/BACKGROUND EVOLVE.png');">

    <div class="background" style="">

        @include('partials.navbar')

        <section class="section1" style="margin-top: 100px;">
            <div class="container">
                <div class="row">
                    <div class="wrapper-flex-center d-flex justify-content-center">
                        <img id="logo-evolty" src="image/EVOLVE/EVOLVE.png" alt="" srcset="">
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="section2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper-flex-center" style="justify-content: center; align-items: center;">
                        <img src="image/tulisan-05.png" alt="" srcset="" style="width: 20%;">
                        <div class="tulisan-1"
                            style="font-size: 6vw; font-family: product_sans; font-weight: bold; letter-spacing: 7.5px;">
                            EVOLVE
                        </div>
                        <img src="image/tulisan-05.png" alt="" srcset="" style="width: 20%; transform: rotate(180deg);">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="background" style="">
        <section class="section3" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="width: auto;">
                                <div class="tulisan-2-judul">
                                    APA ITU EVOLVE?
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="">
                                <span style="">Merupakan subevent baru yang menjadi puncak dari keseluruhan rangkaian
                                    event di Evolty, dengan berisikan talkshow membahas isu seputar teknologi yang
                                    disampaikan
                                    langsung oleh pembicara yang kompeten dan inspiratif, serta terdapat hiburan dengan
                                    bintang tamu.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section4" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    TEMA
                                </div>
                            </div>
                            <div class="tulisan-3-konten" style="text-align:center;">
                                <span style="">"FAST (Festival and Social Technology)" </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section5" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    DETAIL ACARA
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="wrapper-flex-center d-flex justify-content-center justify-content-lg-end">
                            <img id="logo-evolty" src="image/EVOLVE/3.1 TALKSHOW.png" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6" style="text-align:center;">
                        <div class="wrapper-flex-centertl" style="align-items: center; height: 100%;">
                            <div class="wrapper-flex-center-down align-items-lg-start">
                                <div class="tulisan-3-kontenelectra-tl">
                                    Talkshow
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="wrapper-flex-center d-flex justify-content-center justify-content-lg-end">
                            <img id="logo-evolty" src="image/EVOLVE/3.2 EXHIBITION.png" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6" style="text-align:center;">
                        <div class="wrapper-flex-centertl" style="align-items: center; height: 100%;">
                            <div class="wrapper-flex-center-down align-items-lg-start">
                                <div class="tulisan-3-kontenelectra-tl" style="">
                                    Exhibition
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="wrapper-flex-center d-flex justify-content-center justify-content-lg-end">
                            <img id="logo-evolty" src="image/EVOLVE/3.3 GUEST STAR.png" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6" style="text-align:center;">
                        <div class="wrapper-flex-centertl" style="align-items: center; height: 100%;">
                            <div class="wrapper-flex-center-down align-items-lg-start">
                                <div class="tulisan-3-kontenelectra-tl" style="">
                                    Guest Star
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center; ">
                            <div class="tulisan-3-kontenelectra-tl wrapper-flex-center justify-content-center">
                                <span style="">Lokasi Gedung Robotika ITS</span>
                                <!-- gmaps location : https://goo.gl/maps/JFS9B3uChAuiroHw9 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="garis-bawah-judul">
                                <div class="tulisan-2-judul">
                                    GALERI
                                </div>
                            </div>
                            <div class="wrapper-flex-center carousel"
                                style="justify-content: center; margin-top: 30px; width: 70%;">
                                <img src="image/NS/IMG_1566.jpg" alt="" srcset="" style="width: 100%;">
                                <img src="image/NS/IMG_1466.jpg" alt="" srcset="" style="width: 100%;">
                                <img src="image/NS/IMG_1435.jpg" alt="" srcset="" style="width: 100%;">
                                <img src="image/NS/IMG_8057.jpg" alt="" srcset="" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section8" style="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down" style="justify-content: center;">
                            <div class="garis-bawah-judul" style="">
                                <div class="tulisan-2-judul">
                                    DAFTAR SEKARANG!
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <a href="/home">
                                    <div class="wrapper-flex-center justify-content-center">
                                        <img id="logo-evolty" src="image/EVOLVE/5.1 REGISTRASI.png" alt="" srcset="">
                                    </div>
                                </a>
                            </div>
                            <br>
                            <br>
                            <div class="wrapper-flex-center" style="justify-content: center; align-items: center; ">
                                <img src="image/EVOLVE/6. FOOTER BARONAS.png" alt="" srcset="" style="width: 70%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section9" style="margin-top: 150px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="tulisan-1" style="display: flex; justify-content: center;">
                                FIND US
                            </div>
                            <div class="wrapper-flex-center" style="justify-content: center;">
                                <img id="logo-medsos" src="image/logo-03.png" alt="" srcset=""
                                    onclick="location.href='https://www.instagram.com/evolty_its/?hl=id'">
                                <img id="logo-medsos" src="image/logo-05.png" alt="" srcset=""
                                    onclick="location.href='https://twitter.com/evolty_its'">
                                <img id="logo-medsos" src="image/logo-06.png" alt="" srcset=""
                                    onclick="location.href='https://line.me/R/ti/p/%40evolty_its'">
                            </div>
                            <div class="wrapper-flex-center"
                                style="justify-content: center; margin-top: 15px; margin-bottom: 50px;">
                                <img id="logo-berderet" src="image/logo berderet.png" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript" src="js/slickcarousel.js"></script>
    <script type="text/javascript" src="js/navbar2.js"></script>

</body>

</html>