<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="shorcut icon" href="{{asset('image/evolty.png') }}">

    <title>Evolty 2020 - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />

    <!-- Custom styles for this template-->

    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
                <div class="sidebar-brand-text mx-3">Evolty 2020</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0" />

            <!-- Nav Item - Dashboard -->
            <li class="nav-item 
            {{ Request::is('home') || Request::is('admin') ? 'active' : '' }}
            ">
                <a class="nav-link" href="/home">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            @if(Auth::check())
            @if(Auth::user()->is_admin)

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block" />

            <!-- Heading -->
            <div class="sidebar-heading">
                Electra
            </div>

            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/electra') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/electra">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Lihat Peserta</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/electra/tambah') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/electra/tambah">
                    <i class="fas fa-fw fa-plus"></i>
                    <span>Tambah Peserta</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block" />

            <!-- Heading -->
            <div class="sidebar-heading">
                Baronas
            </div>

            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/baronas') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/baronas">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Lihat Peserta</span></a>
            </li>
            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/baronas/tambah') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/baronas/tambah">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Tambah Peserta</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block" />

            <!-- Heading -->
            <div class="sidebar-heading">
                News
            </div>

            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/news') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/news">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Lihat News</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/news/add') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/news/add">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Tambah News</span></a>
            </li>
             <!-- Heading -->
             <div class="sidebar-heading">
                Olimpiade
            </div>

            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/soal') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/soal">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Lihat Soal</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item {{ Request::is('admin/soal/add') ? 'active' : '' }}">
                <a class="nav-link" href="/admin/soal/add">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Tambah Soal</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block" />

            <!-- Heading -->
            <div class="sidebar-heading">
            @endif
            @endif

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block" />

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                                    @if (Auth::check())

                                    {{Auth::user() ->name}}
                                    @endif
                                </span>
                                <i class="fa fa-user"></i>
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    @yield('content')

                    <!-- /.container-fluid -->
                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Evolty 2020</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Logout ?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Apabila anda ingin melakukan logout, silahkan klik tombol logout dibawah ini
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">
                            Cancel
                        </button>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                            <input type="submit" class="btn btn-danger" value="Logout" />
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- Custom scripts for all pages-->
        <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
        <script src="{{ asset('js/datatables-demo3.js') }}"></script>
        <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>

        <script>
            CKEDITOR.replace('news_content', {
                filebrowserUploadUrl: "{{route('ckeditor_upload', ['_token' => csrf_token()])}}",
                filebrowserUploadMethod: 'form',
                uploadUrl: "{{route('ckeditor_upload_json', ['_token' => csrf_token()])}}"
            });
        </script>

</body>

</html>