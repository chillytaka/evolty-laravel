<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shorcut icon" href="image/evolty.png">
    <title>News | Evolty 2020</title>
    <link rel="stylesheet" href="css/style_s.css">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">

</head>

<body style="background-color: black;">

    <div class="background" style="background-image: url('image/BG atas.png'); background-size: 100% 100%;">

        @include('partials.navbar')

        <section class="section1" style="margin-top: 50px;">
            <div class="container">
                <div class="row justify-content-center">
                    @foreach ($news as $e)

                    <a href="/content/{{$e->id}}" class="card mx-3"
                        style="width:18rem;text-decoration:none;color:black">
                        @if ($e->path_headline)
                        <img class="card-img-top" src="news_file/headline/{{$e->path_headline}}" alt="Card image">
                        @endif
                        <div class="card-body">
                            <h4 class="card-title">{{$e->title}}</h4>
                            <p class="card-text text-black-50">{{$e->updated_at}}</p>
                        </div>
                    </a>

                    @endforeach
                </div>
            </div>
        </section>
    </div>

    <div class="background" style="background-image: url('image/BG Bawah.png'); background-size: 100% 100%;">

        <section class="section7" style="margin-top: 150px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper-flex-center-down">
                            <div class="tulisan-1" style="display: flex; justify-content: center;">
                                FIND US
                            </div>
                            <div class="wrapper-flex-center" style="justify-content: center;">
                                <img id="logo-medsos" src="image/logo-03.png" alt="" srcset=""
                                    onclick="location.href='https://www.instagram.com/evolty_its/?hl=id'">
                                <img id="logo-medsos" src="image/logo-05.png" alt="" srcset=""
                                    onclick="location.href='https://twitter.com/evolty_its'">
                                <img id="logo-medsos" src="image/logo-06.png" alt="" srcset=""
                                    onclick="location.href='https://line.me/R/ti/p/%40evolty_its'">
                            </div>
                            <div class="wrapper-flex-center"
                                style="justify-content: center; margin-top: 15px; margin-bottom: 50px;">
                                <img id="logo-berderet" src="image/logo berderet.png" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript" src="js/slickcarousel.js"></script>
    <script type="text/javascript" src="js/navbar2.js"></script>

</body>

</html>