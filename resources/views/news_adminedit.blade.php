@extends('layouts.dashboard')

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Berita</h1>
</div>

<div class="container">
    <form method="post" action="/admin/news/update/{{$news->id}}" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label>Judul Berita</label>
            <input type="text" name="news_title" required class="form-control" value={{$news->title}}
                placeholder="Judul Berita">
        </div>

        <div class="form-group">

            @if($news->path_headline)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/news_file/headline/'.$news->path_headline) }}">
                    lihat file</a>
            </div>
            @endif

            <label>Gambar Headline</label>
            <input type="file" class="form-control-file" name="news_headline_img">
        </div>

        <div class="form-group">
            <label>Main Content</label>
            <textarea name="news_content" id="news_content">{{$content}}</textarea>
        </div>

        <input type="submit" value="submit" class="btn btn-primary">
    </form>
</div>
@endsection