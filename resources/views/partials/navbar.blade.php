<section class="navbar" style="display:flex; align-items: center; margin: 0;">
    <div class="container-fluid">
        <div class="row" style="width: 100%;">
            <!-- Sementara dihardcode karena tidak menemukan masalahnya dimana-->
            <div class="col-lg-12">
                <div class="wrapper-flex-center" id="hamburgerwrapper">
                    <!-- <img id="logo-navbar" src="image/logo home evolty-03.png" alt="" srcset="">                                                                                                                                                                  -->
                    <!-- <nav class="navbar navbar-dark" id="idNavbar" style="height: fit-content; margin-right: 25px; padding: 0; width: fit-content;">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="#navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>                                    
                                </button>                                                                                              
                            </nav> -->
                    <!-- <div class="isi-wrapper" id="isiwrapper" style="">
                                <div class="tulisan-1"><span>Contact Us</span></div>
                                     <div class="dropdown">
                                        <div class="tulisan-1 dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>Event</span></div>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="https://mage-its.com">Mage</a>
                                            <a class="dropdown-item" href="/electra">Electra</a>
                                            <a class="dropdown-item" href="/baronas">Baronas</a>
                                            <a class="dropdown-item" href="/evolve">Evolve</a>
                                    </div>
                                </div>

                                <a href="/login">
                                    <div class="tulisan-1" style="border: 0; font-weight: 500;"><span>Login</span></div>
                                </a>
                            </div>                                                         -->
                </div>
            </div>
        </div>
        <div class="row" style="width: 100%;">
            <div class="col-lg-12">
                <div class="wrapper-flex-center" style="flex-direction: row-reverse;">
                    <div class="">
                        <div class="collapse navbar-collapse" style="" id="navbarSupportedContent1">
                            <a href="/news" style="text-decoration:none;">
                                <div class="tulisan-1" style="font-weight: 500;"><span>News</span></div>
                            </a>

                            <a href="/contact" style="text-decoration:none;">
                                <div class="tulisan-1" style="font-weight: 500;"><span>Contact Us</span></div>
                            </a>
                            <div class="dropdown">
                                <div class="tulisan-1 dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <span>Event</span></div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="https://mage-its.com">Mage</a>
                                    <a class="dropdown-item" href="/electra">Electra</a>
                                    <a class="dropdown-item" href="/baronas">Baronas</a>
                                    <a class="dropdown-item" href="/evolve">Evolve</a>
                                </div>
                            </div>
                            <a href="/login" style="text-decoration:none;">
                                <div class="tulisan-1" style="border: 0; font-weight: 500;"><span>Login</span></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>