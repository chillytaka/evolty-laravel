@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Soal Olimpiade</h1>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>id</th>
                
                <th>soal</th>
                <th>gambar</th>
                <!--<th>pilihan1</th>
                <th>pilihan2</th>
                <th>pilihan3</th>
                <th>pilihan4</th>
                <th>pilihan5</th>-->
                <th>tipesoal</th>
                <th>jawaban</th>
                <th>opsi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($soal as $s)
            <tr>
                <td>{{ $s->id }}</td>
                <td>{{ $s->soal}}</td>
                <td>{{ $s->gambar }}</td>
                <!--<td>{{ $s->pilihan1}}</td>
                <td>{{ $s->pilihan2}}</td>
                <td>{{ $s->pilihan3}}</td>
                <td>{{ $s->pilihan4}}</td>
                <td>{{ $s->pilihan5}}</td>-->
                <td>{{ $s->tipesoal}}</td>
                <td>{{ $s->jawaban }}</td>
                <td>

                    <a href="/admin/soal/edit/{{ $s->id }}" class="btn btn-warning">Edit</a>
                    <a href="/admin/soal/hapus/{{ $s->id }}" class="btn btn-danger">Hapus</a>
                    
                </td>
            </tr>


               
            @endforeach
        </tbody>
    </table>
</div>

@endsection