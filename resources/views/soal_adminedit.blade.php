@extends('layouts.dashboard')


@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Soal</h1>
</div>

<div class="container">

    <form method="post" action="/admin/soal/update/{{ $soal->id }}" enctype="multipart/form-data">

        {{ csrf_field() }}
        

        <div class="form-group">
            <label>Soal</label>
            <textarea name="soal" class="form-control"
                placeholder="Soal .."> {{ $soal->soal }} </textarea>

            @if($errors->has('soal'))
            <div class="text-danger">
                {{ $errors->first('soal')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Pilihan1</label>
            <textarea name="pilihan1" class="form-control"
                placeholder="Pilihan1 .."> {{ $soal->pilihan1 }} </textarea>

            @if($errors->has('pilihan1'))
            <div class="text-danger">
                {{ $errors->first('pilihan1')}}
            </div>
            @endif

        </div>

        <div class="form-group">

            <label>Pilihan2</label>
            <textarea name="pilihan2" class="form-control"
                placeholder="Pilihan2 .."> {{ $soal->pilihan2 }} </textarea>

            @if($errors->has('pilihan2'))
            <div class="text-danger">
                {{ $errors->first('pilihan2')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Pilihan3</label>
            <textarea name="pilihan3" class="form-control"
                placeholder="Pilihan3 .."> {{ $soal->pilihan3 }} </textarea>

            @if($errors->has('pilihan3'))
            <div class="text-danger">
                {{ $errors->first('pilihan3')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Pilihan4</label>
            <textarea name="pilihan4" class="form-control"
                placeholder="Pilihan4 .."> {{ $soal->pilihan4 }} </textarea>

            @if($errors->has('pilihan4'))
            <div class="text-danger">
                {{ $errors->first('pilihan4')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Pilihan5</label>
            <textarea name="pilihan5" class="form-control"
                placeholder="Pilihan5 .."> {{ $soal->pilihan5 }} </textarea>

            @if($errors->has('pilihan5'))
            <div class="text-danger">
                {{ $errors->first('pilihan5')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Tipe soal</label>
            <select name="tipesoal" class="form-control">

                <option value="Matematika" {{$soal->tipesoal === 'Matematika' ? 'selected' : null}}>Matematika</option>

                <option value="Fisika" {{$soal->tipesoal === 'Fisika' ? 'selected' : null}}>
                    Fisika</option>
                
                <option value="Rangkaian Listrik" {{$soal->tipesoal === 'Rangkaian Listrik' ? 'selected' : null}}>
                    Rangkaian Listrik</option>

                <option value="Algoritma pemrograman" {{$soal->tipesoal === 'Algoritma pemrograman' ? 'selected' : null}}>
                    Algoritma pemrograman</option>
                    
            </select>

           
        </div>

       

        <b><img width="300px" src="{{ url('/olimpiade/gambar/'.$soal->gambar) }}"></b>
            
        <div class="form-group">
            <label>
                <b>Gambar</b>
                
            </label>
            @if($soal->gambar)
            
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/olimpiade/gambar/'.$soal->gambar) }}">
                    lihat file</a>
            </div>
            @endif
            <input type="file" name="gambar">
            
            </div>
        </div>


        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>

    </form>

</div>
@endsection