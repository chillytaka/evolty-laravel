@extends('layouts.dashboard')

@section('content')

<div class="container">

    <form method="post" action="/admin/soal/store" enctype="multipart/form-data">

        {{ csrf_field() }}
    
        <div class="form-group">
            <label>Soal</label>
            <input type="text" name="soal" class="form-control" placeholder="Soal..">

            @if($errors->has('soal'))
            <div class="text-danger">
                {{ $errors->first('soal')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Pilihan1</label>
            <input type="text" name="pilihan1" class="form-control" placeholder="Pilihan1 ..">

            @if($errors->has('pilihan1'))
            <div class="text-danger">
                {{ $errors->first('pilihan1')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Pilihan2</label>
            <input type="text" name="pilihan2" class="form-control" placeholder="Pilihan2 ..">

            @if($errors->has('pilihan2'))
            <div class="text-danger">
                {{ $errors->first('pilihan2')}}
            </div>
            @endif

        </div>

        <div class="form-group">

            <label>Pilihan3</label>
            <input type="text" name="pilihan3" class="form-control" placeholder="Pilihan3 ..">

            @if($errors->has('pilihan3'))
            <div class="text-danger">
                {{ $errors->first('pilihan3')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Pilihan4</label>
            <input type="text" name="pilihan4" class="form-control" placeholder="Pilihan4 ..">

            @if($errors->has('pilihan4'))
            <div class="text-danger">
                {{ $errors->first('pilihan4')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>Pilihan5</label>
            <input type="text" name="pilihan5" class="form-control" placeholder="Pilihan5 ..">

            @if($errors->has('pilihan5'))
            <div class="text-danger">
                {{ $errors->first('pilihan5')}}
            </div>
            @endif
        </div>

        <div class="form-group">

            <label>jawaban</label>
            <input type="text" name="jawaban" class="form-control" placeholder="Jawaban ..">

            @if($errors->has('jawaban'))
            <div class="text-danger">
                {{ $errors->first('jawaban')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Tipe Soal</label>
            <select name="tipesoal" class="form-control">
                <option value="Matematika">Matematika</option>
                <option value="Fisika">Fisika</option>
                <option value="Rangkaian Listrik">Rangkaian Listrik</option>
                <option value="Algoritma pemrograman">Algoritma pemrograman</option>
                
            </select>

            @if($errors->has('tipesoal'))
            <div class="text-danger">
                {{ $errors->first('tipesoal')}}
            </div>
            @endif
        </div>

        

        <div class="form-group ">
            <div class="custom-file">
                <label>Gambar</label>
                <input type="file" class="form-control-file" name="gambar">

            </div>

            @if($errors->has('gambar'))
            <div class="text-danger">
                {{ $errors->first('gambar')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>


    </form>

</div>
@endsection