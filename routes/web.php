<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front Page
Route::get('/', function () {
    return view('index');
});

Route::get('/electra', function () {
    return view('electra');
});

Route::get('/baronas', function () {
    return view('baronas');
});

Route::get('/baronas-en', function () {
    return view('baronas-en');
});

Route::get('/evolve', function () {
    return view('evolve');
});

Route::get('/contact', function () {
    return view('contact_us');
});

Route::get('/testing', function () {
    return view('content_news');
});

// End of Front Page

// auth route
Auth::routes();

// File Route
Route::get('/rbSMA',function() {return response()->file(public_path() . '/guidebook/sma.pdf');});
Route::get('/rbSMP',function() {return response()->file(public_path() . '/guidebook/smp.pdf');});
Route::get('/rbSD',function() {return response()->file(public_path() . '/guidebook/sd.pdf');});
Route::get('/rbSumo-ID',function() {return response()->file(public_path() . '/guidebook/sumo(ind).pdf');});
Route::get('/rbSumo-EN',function() {return response()->file(public_path() . '/guidebook/sumo(en).pdf');});
Route::get('/rbUnderwater-ID',function() {return response()->file(public_path() . '/guidebook/rov(ind).pdf');});
Route::get('/rbUnderwater-EN',function() {return response()->file(public_path() . '/guidebook/rov(en).pdf');});
Route::get('/trSMA',function() {
    return response()->download(public_path() . '/guidebook/trackSMA.rar', "trackSMA.rar", 
    ["Content-Disposition" => "attachment, filename='trackSMA.rar'"]);
});
Route::get('/trSMP',function() {
    return response()->download(public_path() . '/guidebook/trackSMP.cdr', "trackSMP.cdr", 
    ["Content-Type" => "image/x-coreldraw", "Content-Disposition" => "attachment, filename='trackSMP.cdr'"]);
});
Route::get('/trSD',function() {
    return response()->download(public_path() . '/guidebook/trackSD.rar', "trackSD.rar", 
    ["Content-Disposition" => "attachment, filename='trackSD.rar'"]);
});
Route::get('/boxSD',function() {
    return response()->download(public_path() . '/guidebook/boxSD.cdr', "boxSD.cdr", 
    ["Content-Type" => "image/x-coreldraw", "Content-Disposition" => "attachment, filename='boxSD.cdr'"]);
});
Route::get('/boxSMP',function() {
    return response()->download(public_path() . '/guidebook/boxSMP.cdr', "boxSMP.cdr", 
    ["Content-Type" => "image/x-coreldraw", "Content-Disposition" => "attachment, filename='boxSMP.cdr'"]);
});
Route::get('/objSMP',function() {
    return response()->download(public_path() . '/guidebook/objSMP.rar', "objSMP.rar", 
    ["Content-Disposition" => "attachment, filename='objSMP.rar'"]);
});
Route::get('/objSMA',function() {
    return response()->download(public_path() . '/guidebook/objSMA.rar', "objSMA.rar", 
    ["Content-Disposition" => "attachment, filename='objSMA.rar'"]);
});

//home user
Route::get('/home', 'HomeController@index');

//news user
Route::get('/news', 'NewsController@index');
Route::get('/content/{id}', 'NewsController@contentShow');

//home admin
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('auth', 'admin');

//electra dashboard user
Route::get('/home/electra/edit', 'ElectraController@edit')->middleware('auth');
Route::post('/home/electra/store', 'ElectraController@store')->middleware('auth');
Route::put('/home/electra/update/{id}', 'ElectraController@update')->middleware('auth');

//baronas dashboard user
Route::get('/home/baronas/edit', 'BaronasController@edit')->middleware('auth');
Route::post('/home/baronas/store', 'BaronasController@store')->middleware('auth');
Route::put('/home/baronas/update/{id}', 'BaronasController@update')->middleware('auth');

//electra dashboard admin 
Route::get('/admin/electra', 'ElectraController@adminindex')->middleware('auth', 'admin');
Route::get('/admin/electra/edit/{id}', 'ElectraController@adminedit')->middleware('auth', 'admin');
Route::get('/admin/electra/tambah', 'ElectraController@admintambah')->middleware('auth', 'admin');
Route::post('/admin/electra/store', 'ElectraController@adminstore')->middleware('auth', 'admin');
Route::put('/admin/electra/update/{id}', 'ElectraController@adminupdate')->middleware('auth', 'admin');
Route::get('/admin/electra/hapus/{id}', 'ElectraController@admindelete')->middleware('auth', 'admin');
Route::put('/admin/electra/bayar/{id}', 'ElectraController@adminpay')->middleware('auth', 'admin');
Route::get('/admin/electra/cari', 'ElectraController@admincari');
Route::get('/admin/electra/csv', 'ElectraController@downloadCsv')->middleware('auth', 'admin');

//baronas dashboard admin
Route::get('/admin/baronas', 'BaronasController@adminindex')->middleware('auth', 'admin');
Route::put('/admin/baronas/bayar/{id}', 'BaronasController@adminpay')->middleware('auth', 'admin');
Route::get('/admin/baronas/edit/{id}', 'BaronasController@adminedit')->middleware('auth', 'admin');
Route::get('/admin/baronas/hapus/{id}', 'BaronasController@admindelete')->middleware('auth', 'admin');
Route::get('/admin/baronas/csv', 'BaronasController@downloadCsv')->middleware('auth', 'admin');
Route::get('/admin/baronas/tambah', 'BaronasController@admintambah')->middleware('auth', 'admin');
Route::post('/admin/baronas/store', 'BaronasController@adminstore')->middleware('auth', 'admin');

//news editor admin
Route::get('/admin/news/add', function () {
    return view('news_admintambah');
})->middleware('auth', 'admin');
Route::get('/admin/news', 'NewsController@adminindex')->middleware('auth', 'admin');
Route::get('/admin/news/edit/{id}', 'NewsController@adminedit')->middleware('auth', 'admin');
Route::get('/admin/news/hapus/{id}', 'NewsController@admindelete')->middleware('auth', 'admin');
Route::post('/admin/news/update/{id}', 'NewsController@update')->middleware('auth', 'admin');
Route::post('/admin/news/store', 'NewsController@store')->middleware('auth', 'admin');
Route::post('/admin/news/upload', 'NewsController@uploader')->middleware('auth', 'admin')->name('ckeditor_upload');
Route::post('/admin/news/upload-json', 'NewsController@uploaderJson')->middleware('auth', 'admin')->name('ckeditor_upload_json');

//olimpiade admin
Route::get('/admin/soal', 'SoalController@index')->middleware('auth', 'admin');
Route::get('/admin/soal/add', 'SoalController@tambah')->middleware('auth', 'admin');
Route::post('/admin/soal/store', 'SoalController@store')->middleware('auth', 'admin');
Route::get('/admin/soal/edit/{id}', 'SoalController@edit')->middleware('auth', 'admin');
Route::post('/admin/soal/update/{id}', 'SoalController@update')->middleware('auth', 'admin');
Route::get('/admin/soal/hapus/{id}', 'SoalController@delete')->middleware('auth', 'admin');
Route::get('/admin/soal/random', 'SoalController@random')->middleware('auth', 'admin');
